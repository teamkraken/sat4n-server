const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const JwtStrategy = require('passport-jwt').Strategy
const { ExtractJwt } = require('passport-jwt')
const { authController } = require('../controllers')
const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const config = yaml.load(fs.readFileSync(path.join(__dirname, '../config/config.yaml')), 'utf8')

const JwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.JWT_SECRET
  // issuer: 'localhost',
  // audience: 'localhost'
}

passport.use(new LocalStrategy(authController.localLogin))

passport.use(new JwtStrategy(JwtOptions, authController.jwtLogin))
