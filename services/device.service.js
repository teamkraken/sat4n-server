const Op = require('sequelize').Op
const { audit, device, port, vlan } = require('../models')
const { Cisco } = require('../services')

module.exports = {
  /**
   * List returns a list of devices
   *
   * @returns {array} array of devices
   */
  list: async () => {
    return await device.findAll({
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      },
      order: [['name', 'ASC']]
    })
      .then((devices) => { return devices })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  /**
   * Gets device based on the ID
   *
   * @param {number} id
   * @returns {object} Device
   */
  get: async (id) => {
    const dev = await device.findOne({
      where: { id: { [Op.eq]: id } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt'] }
        },
        {
          model: vlan,
          as: 'vlans',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          through: { attributes: [] }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then((d) => {
        if (d === null) {
          throw new Error('Device not found')
        }

        return d
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Implemented for testing, wasn't fully returning on reject
    if (dev === null) {
      throw new Error('Device not found')
    }

    // Fetch info from device
    const response = await Cisco.getInfo(dev.ip)
      .then((device) => { return device })
      .catch(() => { throw new Error(`Unable to establish connection to ${dev.ip}`) })

    // Implemented for testing, wasn't fully returning on reject
    if (response === undefined) {
      throw new Error(`Unable to establish connection to ${dev.ip}`)
    }

    // Create device in database
    await dev.update({ ...response.info }, { where: { id: { [Op.eq]: id } } })
      .then((device) => { return device })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Iterate through ports and update only ports where vlan or status has changed
    await response.interfacesStatus.map(async (p, i) => {
      const foundPort = await response.interfaces.filter((int) => {
        const firstHalf = int.interface.slice(0, 2)
        const secondHalf = int.interface.replace(/[a-zA-Z]/g, '')
        const name = `${firstHalf}${secondHalf}`

        return name === p.name
      })

      await port.update(
        {
          ...p,
          lastUsed: foundPort[0].lastInput,
          inputErrors: foundPort[0].inputErrors,
          outputErrors: foundPort[0].outputErrors
        },
        { where: { id: { [Op.eq]: dev.ports[i].id } } }
      )
        .catch(/* istanbul ignore next */ (err) => { throw err })
    })

    // Format vlans for db entry
    const newVlans = response.vlans.map((vlan) => { return { id: vlan.vlan, name: vlan.name } })

    // Get diff of vlans from switch and db
    const vlansDiff = newVlans.filter((nv) => dev.vlans.map((v) => v.id).indexOf(parseInt(nv.id)) === -1)

    /* istanbul ignore next */
    if (newVlans.length !== dev.vlans.length && vlansDiff.length !== 0) {
      // Try to insert vlans into db
      const vlans = await vlan.bulkCreate(newVlans, { returning: true, ignoreDuplicates: true })
        .catch(/* istanbul ignore next */ (err) => { throw err })
      // Associate vlans to device
      await dev.setVlans(vlans) // Sequelize deletes associations before setting assiciations like this (O.o)
        .catch(/* istanbul ignore next */ (err) => { throw err })
    }

    // Fetch updated device
    const returnDevice = await device.findOne({
      where: { id: { [Op.eq]: id } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt', 'deviceId'] }
        },
        {
          model: vlan,
          as: 'vlans',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          through: { attributes: [] }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then((d) => { return d })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Return device
    return ({
      ...JSON.parse(JSON.stringify(returnDevice)), // Stripping away circular references
      arpTable: response.arpTable,
      ipSnoop: response.ipSnoop,
      macTable: response.macTable,
      neighbors: response.neighbors,
      log: response.log
    })
  },

  /**
   * Creates a device based on the IP
   *
   * @param {string} ip ip of the device
   * @param {object} user an object with id and username
   * @returns {object} Device
   */
  create: async (ip, user) => {
    // Check if device already exists
    const existingDevice = await device.findOne({ where: { ip: { [Op.eq]: ip } } })
      .then((d) => {
        if (d !== null) {
          throw new Error('Device with this IP already exists')
        }

        return d
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Implemented for testing, wasn't fully returning on reject
    if (existingDevice !== null) {
      throw new Error('Device with this IP already exists')
    }

    // Fetch info from device
    const response = await Cisco.getInfo(ip)
      .then((d) => { return d })
      .catch(() => { throw new Error(`Unable to establish connection to ${ip}`) })

    // Implemented for testing, wasn't fully returning on reject
    if (response === undefined) {
      throw new Error(`Unable to establish connection to ${ip}`)
    }

    // Create device in database
    const newDevice = await device.create({ ...response.info, ip })
      .then(async (d) => {
        const newAudit = {
          userId: user.id,
          action: 'Device:CREATE',
          details: `${user.username} created device with ip: ${d.ip} and name: ${d.name}`,
          deviceId: d.id
        }

        // Create audit log
        await audit.create(newAudit)
          .catch(/* istanbul ignore next */ (err) => { throw err })

        return d
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Format vlans for db entry
    const newVlans = response.vlans.map((vlan) => { return { id: vlan.vlan, name: vlan.name } })
    const vlans = await vlan.bulkCreate(newVlans, { returning: true, ignoreDuplicates: true })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Associate vlans to device
    await newDevice.setVlans(vlans)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Create interfaces (ports) in database
    const newPorts = await port.bulkCreate(response.interfacesStatus, { returning: true })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Associate ports to device
    newDevice.setPorts(newPorts)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Fetch new device
    const returnDevice = await device.findOne({
      where: { id: { [Op.eq]: newDevice.id } },
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then((d) => { return d })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    return returnDevice
  },

  /**
   * Deletes device based on ID
   *
   * @param {number} id device ID
   * @param {object} user object with id and username
   * @returns 1 if it succeeds
   */
  delete: async (id, user) => {
    const d = await device.findOne({ where: { id: { [Op.eq]: id } } })
      .then((ret) => {
        if (ret === null) {
          throw new Error('Device not found')
        }

        return ret
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    return await device.destroy({ where: { id: { [Op.eq]: id } } })
      .then(async (success) => {
        const newAudit = {
          userId: user.id,
          action: 'Device:DELETE',
          details: `${user.username} deleted device with ip: ${d.ip} and name: ${d.name}`
        }
        return await audit.create(newAudit)
          .then(() => { return success })
          .catch(/* istanbul ignore next */ (err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  }
}
