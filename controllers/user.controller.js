const service = require('../services/user.service')
const { hasAccess } = require('../utils')

module.exports = {
  list: async (req, res, next) => {
    return await hasAccess('users_list', req.user.role.id)
      .then(async () => {
        return await service.list()
          .then((users) => { return res.status(200).json(users) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  get: async (req, res, next) => {
    return await hasAccess('users_get', req.user.role.id)
      .then(async () => {
        return await service.get(req.params.id)
          .then((user) => { return res.status(200).json(user) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  create: async (req, res, next) => {
    const { username, password } = req.body

    if (username === '' || password === '') {
      return res.status(400).json('Bad Request')
    }

    return await hasAccess('users_create', req.user.role.id)
      .then(async () => {
        return await service.create(req.body, req.user.user)
          .then((user) => { return res.status(201).json(user) })
          .catch((err) => { return res.status(412).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  update: async (req, res, next) => {
    const { username } = req.body

    if (username === '') {
      return res.status(400).json('Bad Request')
    }

    return await hasAccess('users_update', req.user.role.id)
      .then(async () => {
        return await service.update(req.body, req.params.id, req.user.user)
          .then((user) => { return res.status(200).json(user) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  delete: async (req, res, next) => {
    return await hasAccess('users_delete', req.user.role.id)
      .then(async () => {
        return await service.delete(req.params.id, req.user.user)
          .then((success) => { return res.status(200).json(success) })
      })
      .catch((err) => { return res.status(403).json(err) })
  }
}
