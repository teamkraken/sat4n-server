const fs = require('fs')
const path = require('path')
const Sequelize = require('sequelize')
const env = process.env.NODE_ENV || 'development'
const yaml = require('js-yaml')
const config = yaml.load(fs.readFileSync(path.join(__dirname, '../config/config.yaml')), 'utf8').sequelize[env]

let sequelize
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config)
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config)
}

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.apiResource = require('./api.resource')(sequelize, Sequelize)
db.audit = require('./audit')(sequelize, Sequelize)
db.device = require('./device')(sequelize, Sequelize)
db.port = require('./port')(sequelize, Sequelize)
db.role = require('./role')(sequelize, Sequelize)
db.roleApiResource = require('./role.api.resource')(sequelize, Sequelize)
db.user = require('./user')(sequelize, Sequelize)
db.userSecret = require('./usersecret')(sequelize, Sequelize)
db.vlan = require('./vlan')(sequelize, Sequelize)

db.apiResource.belongsToMany(db.role, {
  through: 'roleApiResource',
  as: 'roles'
})

db.audit.belongsTo(db.user, {
  as: 'user',
  allowNull: false
})

db.audit.belongsTo(db.device, {
  as: 'device',
  allowNull: true
})

db.device.belongsToMany(db.vlan, {
  through: 'deviceVlan',
  as: 'vlans'
})

db.device.hasMany(db.port, {
  as: 'ports'
})

db.port.belongsTo(db.device)

db.role.belongsToMany(db.apiResource, {
  through: 'roleApiResource',
  as: 'resources'
})

db.role.hasMany(db.user, {
  as: 'users'
})

db.user.hasMany(db.userSecret)

db.user.belongsTo(db.role)

db.userSecret.belongsTo(db.user)

db.vlan.belongsToMany(db.device, {
  through: 'deviceVlan',
  as: 'devices'
})

module.exports = db
