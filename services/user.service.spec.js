/* eslint-env jest */
const service = require('./user.service')

describe('User Service', () => {
  const creator = {
    id: 1,
    username: 'admin'
  }

  it('Should return the new user', async () => {
    const user = await service.create({
      username: 'user',
      password: 'user',
      roleId: 1
    }, creator)

    // The user should be user
    expect(user.username).toBe('user')
  })

  it('Should return error when trying to add an existing user', async () => {
    const user = service.create({
      username: 'user',
      password: 'user',
      roleId: 1
    }, creator)

    // The user should be user
    await expect(user).rejects.toThrow(new Error('User with this username already exists'))
  })

  it('Should return a list of users', async () => {
    const users = await service.list()

    // Get a list of users
    expect(users.length).toBeGreaterThan(0)

    // The first user should be admin
    expect(users[0].username).toBe('admin')
  })

  it('Should return user with ID of 1', async () => {
    const user = await service.get(1)

    // The first user should be admin
    expect(user.username).toBe('admin')
  })

  it('Should return the updated user', async () => {
    const users = await service.list()
    const user = users.filter((u) => u.username === 'user')
    expect(user.length).toBe(1)

    const newUser = await service.update({
      id: user[0].id,
      username: user[0].username,
      password: 'asdf',
      roleId: user[0].roleId
    }, user[0].id, creator)

    // The user should be user
    expect(newUser.username).toBe('user')
  })

  it('Should return the ID of deleted user', async () => {
    const users = await service.list()
    const user = users.filter((u) => u.username === 'user')
    expect(user.length).toBe(1)

    const success = await service.delete(user[0].id, creator)

    // The user should be user
    expect(success).toBe(1)
  })
})
