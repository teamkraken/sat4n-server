const service = require('../services/audit.service')
const { hasAccess } = require('../utils')

module.exports = {
  list: async (req, res, next) => {
    return await hasAccess('audit_get', req.user.role.id)
      .then(async () => {
        return await service.list()
          .then((logs) => { return res.status(200).json(logs) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  getByUserId: async (req, res, next) => {
    return await hasAccess('audit_get', req.user.role.id)
      .then(async () => {
        return await service.getByUserId(req.params.id)
          .then((logs) => { return res.status(200).json(logs) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  getByDeviceId: async (req, res, next) => {
    return await hasAccess('audit_get', req.user.role.id)
      .then(async () => {
        return await service.getByDeviceId(req.params.id)
          .then((logs) => { return res.status(200).json(logs) })
      })
      .catch((err) => { return res.status(403).json(err) })
  }
}
