/* eslint-env jest */
const service = require('./auth.service')

describe('Auth Service', () => {
  it('Should return user on successful login', async () => {
    expect.assertions(3)
    const login = await service.localLogin({
      username: 'admin',
      password: 'admin'
    })

    expect(login.id).toBe(1)
    expect(login.username).toBe('admin')
    expect(login.role.name).toBe('Administrators')
  })

  it('Should return error message on failed login', async () => {
    // expect.assertions(1)
    const login = service.localLogin({
      username: 'admin',
      password: 'admi'
    })

    await expect(login).rejects.toThrow(new Error('Invalid username or password'))
  })

  it('Should return user on successful JWT login', async () => {
    expect.assertions(1)
    const login = await service.jwtLogin({
      user: {
        username: 'admin'
      }
    })

    expect(login.user.username).toBe('admin')
  })

  it('Should return error message on failed JWT login', async () => {
    expect.assertions(1)
    const login = service.jwtLogin({
      user: {
        username: ''
      }
    })

    await expect(login).rejects.toEqual(new Error('401: Invalid user'))
  })
})
