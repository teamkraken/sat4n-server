const Op = require('sequelize').Op
const { audit, apiResource, role, roleApiResource } = require('../models')

module.exports = {
  list: async () => {
    const roles = await role.findAll({
      attributes: ['id', 'name', 'description']
    })
      .then((roles) => { return roles })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    return roles
  },

  get: async (id) => {
    return await role.findOne({
      include: [{
        model: apiResource,
        as: 'resources',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }],
      where: { id: { [Op.eq]: id } },
      attributes: ['id', 'name', 'description']
    })
      .then(async (role) => {
        if (role === null) {
          throw new Error('Role not found')
        }

        return await apiResource.findAll()
          .then((resources) => {
            return ({
              id: role.id,
              name: role.name,
              description: role.description,
              resources: resources.map((resource) => {
                return {
                  id: resource.id,
                  name: resource.name,
                  enabled: role.resources.filter(o => o.id === resource.id).length !== 0
                }
              })
            })
          })
          .catch(/* istanbul ignore next */ (err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  create: async (newRole, user) => {
    // Check if role already exists
    await role.findOne({ where: { name: { [Op.eq]: newRole.name } } })
      .then((r) => {
        if (r !== null) {
          throw new Error('Role with this name already exists')
        }
      })

    // Remove id received for new role
    delete newRole.id

    return await role.create(newRole)
      .then(async (r) => {
        return await role.findOne({
          where: { id: { [Op.eq]: r.id } },
          attributes: ['id', 'name', 'description']
        })
          .then(async (d) => {
            const newAudit = {
              userId: user.id,
              action: 'Role:CREATE',
              details: `${user.username} created role with name: ${newRole.name}`
            }
            return await audit.create(newAudit)
              .then(() => { return d })
              .catch((err) => { throw err })
          })
      })
      .catch((err) => { throw err })
  },

  update: async (newRole, roleID, user) => {
    // Remove id received in case it is not the same as roleID
    delete newRole.id

    const oldRole = await role.findOne({
      where: { id: { [Op.eq]: roleID } },
      include: [{
        model: apiResource,
        as: 'resources',
        required: false,
        attributes: ['id', 'name'],
        through: { attributes: [] }
      }],
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      }
    })

    if (oldRole === null) {
      throw new Error('Role not found')
    }

    return await role.update(newRole,
      { where: { id: { [Op.eq]: roleID } } })
      .then(async (r) => {
        const addRelation = []
        const removeRelation = []

        await newRole.resources.forEach((resource) => {
          const obj = { roleId: roleID, apiResourceId: resource.id }
          resource.enabled ? /* istanbul ignore next */ addRelation.push(obj) : removeRelation.push(obj)
        })

        return await roleApiResource.bulkCreate(addRelation, { returning: true, ignoreDuplicates: true })
          .then(() => {
            return roleApiResource.destroy({
              where: {
                roleId: { [Op.in]: removeRelation.map(d => { return d.roleId }) },
                apiResourceId: { [Op.in]: removeRelation.map(d => { return d.apiResourceId }) }
              }
            })
              .then(async () => {
                const newAudit = {
                  userId: user.id,
                  action: 'Role:UPDATE',
                  details: `${user.username} updated role with name: ${newRole.name}.
                  \nRole before update: ${JSON.stringify(oldRole)}\n
                  \nRole after update: ${JSON.stringify(newRole)}`
                }
                return await audit.create(newAudit)
                  .then(async () => {
                    // Fetch updated user to return
                    return await role.findOne({
                      where: { id: { [Op.eq]: roleID } },
                      attributes: ['id', 'name', 'description']
                    })
                      .then((u) => { return u })
                  })
                  .catch((err) => { throw err })
              })
              .catch(/* istanbul ignore next */ (err) => { throw err })
          })
          .catch(/* istanbul ignore next */ (err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  delete: async (id, user) => {
    const r = await role.findOne({ where: { id: { [Op.eq]: id } } })
      .then((ret) => { return ret })
      .catch((err) => { throw err })

    if (r === null) {
      throw new Error('Role not found')
    }

    return await role.destroy({
      where: { id: { [Op.eq]: id } }
    })
      .then(async (success) => {
        const newAudit = {
          userId: user.id,
          action: 'Role:DELETE',
          details: `${user.username} deleted role with name: ${r.name}`
        }
        return await audit.create(newAudit)
          .then(() => { return success })
          .catch((err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  }
}
