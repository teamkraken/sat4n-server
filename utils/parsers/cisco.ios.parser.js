const normalizeMAC = (mac) => {
  return mac.toUpperCase().replace(/[:.]/g, '').match(/.{2}/g).join(':')
}

class Parser {
  constructor () {
    this.name = 'Cisco'
    this.description = ''
    this.commands = {
      cdpNeighbors: [
        'terminal length 0',
        'show cdp neighbors'
      ],

      interfacesStatus: [
        'terminal length 0',
        'show interfaces status'
      ]
    }
  }

  arpTable (response) {
    const arpAddresses = []

    const data = response.split('\r\n').slice(2, -1) // Split on new line

    data.forEach(function (element) {
      const arp = {
        protocol: element.substr(0, 10).trim(),
        address: element.substr(10, 17).trim(),
        age: element.substr(27, 11).trim(),
        macAddress: normalizeMAC(element.substr(38, 16).trim()),
        type: element.substr(54, 7).trim(),
        interface: element.substr(61).trim()
      }

      arpAddresses.push(arp)
    })

    return arpAddresses
  }

  cdpNeighbors (response) {
    const devices = []
    let device = {}
    let deviceID = ''

    const data = response.split('\r\n').slice(6, -1) // Split on new line

    data.forEach(function (element) {
      if (element.length < 70) {
        deviceID = element.trim()
      } else {
        device = {
          device: element.substr(0, 17).trim().length === 0 ? deviceID : element.substr(0, 17).trim(),
          localPort: element.substr(17, 18).trim(),
          holdTime: element.substr(35, 11).trim(),
          capability: element.substr(46, 12).trim(),
          platform: element.substr(58, 10).trim(),
          remotePort: element.substr(68).trim()
        }

        devices.push(device)
        device = {}
      }
    })

    return devices
  }

  interfaces (response) {
    const data = response.split('\r\n').slice(1, -1)
    const interfaces = []

    while (data.length > 0) {
      // Find where interface begins
      const start = data.findIndex((d) => { return d.indexOf(' ') !== 0 })
      // Find where next interface begins
      const end = data.slice(start + 1, data.length).findIndex((d) => { return d.indexOf(' ') !== 0 }) + 1
      const int = {}

      let rawInterface = data.splice(0, end !== 0 ? end : data.length)

      rawInterface = rawInterface.join(',')
        .split(/[,;]/)
        .filter((d) => { return d.trim() !== '' }) // Remove empty arrays
        .map((d) => { return d.trim() }) // Trim string in array

      rawInterface.forEach((item, i) => {
        item = item.split(' ')

        if (i === 0) {
          int.interface = item[0]
          int.status = item.slice(2, item.length).join(' ')
        } else if (i === 1) {
          int.lineProtocol = item.slice(3, item.length).join(' ')
        } else if (item.join(' ').indexOf('Hardware is not present') === 0) {
          // console.log('HIT!')
        } else if (item.join(' ').indexOf('Hardware is') === 0 && item.join(' ').indexOf('not present') === -1) {
          int.hardware = item.slice(2, item.length).join(' ')
        } else if (item.indexOf('reliability') === 0) {
          int.reliability = item[1]
        } else if (item.indexOf('txload') === 0) {
          int.txload = item[1]
        } else if (item.indexOf('rxload') === 0) {
          int.rxload = item[1]
        } else if (item.join(' ').indexOf('Last input') === 0) {
          int.lastInput = item[2]
        } else if (item.indexOf('output') === 0 && item.join(' ').indexOf('output hang') === -1) {
          int.output = item[1]
        } else if (item.join(' ').indexOf('input errors') !== -1) {
          int.inputErrors = item[0]
        } else if (item.join(' ').indexOf('output errors') !== -1) {
          int.outputErrors = item[0]
        } else if (item.join(' ').indexOf('5 minute') !== -1) {
          const key = item[2] + item.slice(3, 4).map((l) => { return l.charAt(0).toUpperCase() + l.slice(1) }).join('')
          int[key] = item[4]
        } else if (!isNaN(item[1])) {
          int[item[0].toLowerCase()] = item[1]
        } else if (!isNaN(item[0]) && item.length === 2) {
          int[item[1].toLowerCase()] = item[0]
        } else if (!isNaN(item[0])) {
          // Capitalize items in array after first item
          const key = item[1] + item.slice(2).map((l) => { return l.charAt(0).toUpperCase() + l.slice(1) }).join('')
          int[key] = item[0]
        }
      })

      interfaces.push(int)
    }

    return interfaces
  }

  interfacesStatus (response) {
    const header = response
      .split('\r\n').slice(2, 3)[0]
      .split(/(\w+\s+)/) // Split on word and space
      .filter(h => h.length !== 0) // Remove empty
      .map(h => h.length) // Change to the character length of string
    const lines = response.split('\r\n').slice(3, -1)

    return lines.map((line) => {
      return {
        name: line.substr(0, header.slice(0, 1).reduce((a, b) => a + b, 0)).trim(),
        description: line.substr(header.slice(0, 1).reduce((a, b) => a + b, 0), header[1]).trim(),
        status: line.substr(header.slice(0, 2).reduce((a, b) => a + b, 0), header[2]).trim(),
        vlan: line.substr(header.slice(0, 3).reduce((a, b) => a + b, 0), header[3]).trim(),
        duplex: line.substr(header.slice(0, 4).reduce((a, b) => a + b, 0), header[4] - 1).trim(),
        speed: line.substr(header.slice(0, 5).reduce((a, b) => a + b, 0) - 1, header[5]).trim(),
        type: line.substr(header.slice(0, 6).reduce((a, b) => a + b, 0)).trim()
      }
    })
  }

  ipSnoop (response) {
    const ipAddresses = []

    const data = response.split('\r\n').slice(3, -3) // Split on new line

    data.forEach(function (element) {
      const ip = {
        macAddress: element.substr(0, 20).trim().toUpperCase().replace(/[:.]/g, '').match(/.{2}/g).join(':'),
        ipAddress: element.substr(20, 17).trim(),
        lease: element.substr(37, 12).trim(),
        type: element.substr(49, 15).trim(),
        vlan: element.substr(64, 6).trim(),
        interface: element.substr(70).trim()
      }

      ipAddresses.push(ip)
    })

    return ipAddresses
  }

  macAddressTable (response) {
    const macAddresses = []

    const data = response.split('\r\n').slice(6, -2) // Split on new line

    data.forEach(function (element) {
      const mac = {
        vlan: element.substr(0, 8).trim(),
        macAddress: normalizeMAC(element.substr(8, 18).trim()),
        type: element.substr(26, 12).trim(),
        ports: element.substr(38).trim()
      }

      macAddresses.push(mac)
    })

    return macAddresses
  }

  parse (data) {
    switch (data.command) {
      case 'show interfaces':
        return this.interfaces(data.response)
      case 'show interfaces status':
        return this.interfacesStatus(data.response)
      case 'show vlan brief':
        return this.vlansBrief(data.response)
      case 'show cdp neighbors':
        return this.cdpNeighbors(data.response)
      case 'show mac address-table | exclude CPU':
        return this.macAddressTable(data.response)
      case 'show ip dhcp snooping binding':
        return this.ipSnoop(data.response)
      case 'show ip arp':
        return this.arpTable(data.response)
      case 'show version':
        return this.version(data.response)
      default:
        return data.response.split('\r\n').slice(1, -1)
    }
  }

  version (response) {
    let tmp = ''
    const lines = response.split('\r\n').slice(1, -1)

    // Find line with hostname and uptime
    tmp = lines.filter(d => d.indexOf('uptime') !== -1)[0].split('uptime is')
    const name = tmp[0].trim()
    const uptime = tmp[1].trim()

    // Find line with device info
    tmp = lines.filter(d => d.indexOf('RELEASE SOFTWARE') !== -1)[0]
    const os = tmp.split(',')[0].trim()
    const osVersion = tmp.split(',')[2].trim() // tmp[4].trim()
    const osImage = tmp.split(',')[1].trim() // tmp[5].trim()

    // Find line with model info
    const model = lines.filter(d => d.indexOf('Model number') !== -1 || d.indexOf('Model Number') !== -1)[0].split(':')[1].trim()

    const ret = {
      name,
      uptime,
      model,
      os,
      osVersion,
      osImage
    }

    return ret
  }

  vlansBrief (response) {
    const vlans = []
    let vlan = {}
    response = response.split('\r\n').slice(2, -1) // Split on new line
    // response = response.shift() // Split on new line
    response = response.slice(2)

    response.forEach(function (element) {
      if (element.substr(0, 5).trim().length === 0) {
        vlan.ports += ', ' + element.substr(48)
      } else {
        if (Object.keys(vlan).length !== 0 && vlan.constructor === Object) { // Prevent adding empty objects
          vlans.push(vlan)
        }

        vlan = {
          vlan: element.substr(0, 5).trim(),
          name: element.substr(5, 33).trim(),
          status: element.substr(38, 10).trim(),
          ports: element.substr(48).trim()
        }
      }
    })

    vlans.push(vlan) // Push the last object

    return vlans.filter(l => ['1002', '1003', '1004', '1005'].indexOf(l.vlan) === -1)
  }
}

module.exports = new Parser()
