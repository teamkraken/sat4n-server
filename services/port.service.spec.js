/* eslint-env jest */
const service = require('./port.service')
const deviceService = require('./device.service')
const ssh = require('./ssh.service')

jest.mock('./ssh.service')

const mockDevice = {
  interfaces: [
    {
      interface: 'GigabitEthernet0/1',
      output: '00:00:00',
      inputErrors: '0',
      outputErrors: '0'
    },
    {
      interface: 'GigabitEthernet0/2',
      output: '00:00:00',
      inputErrors: '0',
      outputErrors: '0'
    },
    {
      interface: 'GigabitEthernet0/3',
      output: '00:00:00',
      inputErrors: '0',
      outputErrors: '0'
    }
  ],
  interfacesStatus: [
    {
      name: 'Gi0/1',
      description: '',
      status: 'connected',
      vlan: '1',
      duplex: 'a-full',
      speed: 'a-1000',
      type: '10/100/1000BaseTX'
    },
    {
      name: 'Gi0/2',
      description: '',
      status: 'connected',
      vlan: 'trunk',
      duplex: 'a-full',
      speed: 'a-1000',
      type: '10/100/1000BaseTX'
    },
    {
      name: 'Gi0/3',
      description: '',
      status: 'disabled',
      vlan: '1',
      duplex: 'a-full',
      speed: 'a-1000',
      type: '10/100/1000BaseTX'
    }
  ],
  vlans: [
    { vlan: '1', name: 'Default', status: 'active', ports: '' },
    { vlan: '666', name: 'Kraken', status: 'active', ports: '' }
  ],
  neighbors: [
    {
      device: 'SEPD4AD717F047C',
      localPort: 'Fas 0/2',
      holdTime: '156',
      capability: 'H P M',
      platform: 'IP Phone',
      remotePort: 'Port 1'
    }
  ],
  macTable: [
    {
      vlan: '1',
      macAddress: '1A:2C:3A:4E:00:F2',
      type: 'DYNAMIC',
      ports: 'Gi0/1'
    },
    {
      vlan: '200',
      macAddress: 'D4:AD:71:7F:04:7C',
      type: 'STATIC',
      ports: 'Fa0/2'
    }
  ],
  ipSnoop: [
    {
      macAddress: '1A:2C:3A:4E:00:F2',
      ipAddress: '10.100.15.3',
      lease: '83578',
      type: 'dhcp-snooping',
      vlan: '1',
      interface: 'GigabitEthernet0/1'
    }
  ],
  arpTable: [
    {
      protocol: 'Internet',
      address: '10.100.15.1',
      age: '-',
      macAddress: '00:22:91:C6:3C:40',
      type: 'ARPA',
      interface: 'Vlan1'
    }
  ],
  info: {
    name: 'c3560-test-1',
    uptime: '1 week, 48 minutes',
    model: 'WS-C3560-8PC',
    os: 'Cisco IOS Software',
    osVersion: '12.2(55)SE4',
    osImage: 'C3560-IPSERVICESK9-M',
    portCount: '9',
    location: 'Eir5;2E;Nethopur'
  },
  log: []
}

describe('Port Service', () => {
  const user = { id: 1, username: 'Administrator' }
  let device

  beforeAll(async () => {
    ssh.getInfo.mockResolvedValue(mockDevice)
    device = await deviceService.create('0.0.0.3', user)
    device = await deviceService.get(device.id)
  })

  afterAll(async () => {
    await deviceService.delete(device.id, user)
  })

  /* BEGIN RESTART PORT */
  it('Should restart port by deviceID and portID', async () => {
    ssh.restartPort.mockResolvedValue(mockDevice)

    await service.restart(device.id, device.ports[0].id, user).then((d) => {
      expect(d).toEqual(1)
    })
  })

  it('Should receive error when restarting a trunk port', async () => {
    ssh.restartPort.mockResolvedValue(mockDevice)

    await service.restart(device.id, device.ports[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Cannot restart trunks or disabled ports'))
    })
  })

  it('Should receive error when restarting a port that does not belong to device', async () => {
    ssh.restartPort.mockRejectedValue()

    await service.restart(device.id, device.ports[0].id + 10, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })

  it('Should receive error when restarting a port on a device that does not exist', async () => {
    ssh.restartPort.mockRejectedValue()

    await service.restart(device.id + 10, device.ports[0].id, user).catch((err) => {
      expect(err).toEqual(new Error('Device not found'))
    })
  })
  /* END RESTART PORT */

  /* BEGIN DISABLE PORT */
  it('Should receive device after port is disabled', async () => {
    ssh.disablePort.mockResolvedValue(mockDevice)

    await service.disable(device.id, device.ports[0].id, user).then((d) => {
      expect(d.ip).toEqual(device.ip)
    })
  })

  it('Should error when trying to disable a disabled or a trunk port', async () => {
    ssh.disablePort.mockRejectedValue()

    // Disabled port
    await service.disable(device.id, device.ports[2].id, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })

    // Trunk port
    await service.disable(device.id, device.ports[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })

  it('Should receive error when trying to disable a port that does not exist', async () => {
    ssh.disablePort.mockRejectedValue()

    await service.disable(device.id, device.ports[0].id + 10, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })
  /* END DISABLE PORT */

  /* START ENABLE PORT */
  it('Should receive device after port is enabled', async () => {
    ssh.enablePort.mockResolvedValue(mockDevice)

    await service.enable(device.id, device.ports[2].id, user).then((d) => {
      expect(d.ip).toEqual(device.ip)
    })
  })

  it('Should error when trying to enable a trunk port', async () => {
    ssh.enablePort.mockRejectedValue()

    // Trunk port
    await service.enable(device.id, device.ports[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })

  it('Should receive error when trying to enable a port that does not exist', async () => {
    ssh.enablePort.mockRejectedValue()

    await service.enable(device.id, device.ports[0].id + 10, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })
  /* END ENABLE PORT */

  /* BEGIN UPDATE PORT */
  it('Should receive device after port is updated', async () => {
    ssh.updatePort.mockResolvedValue(mockDevice)

    await service.update(device.id, device.ports[0].id, device.vlans[1].id, user).then((d) => {
      expect(d.ip).toEqual(device.ip)
    })
  })

  it('Should receive error when trying to update a port that does not exist', async () => {
    ssh.updatePort.mockRejectedValue()

    await service.update(device.id, device.ports[0].id + 10, device.vlans[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Port not found'))
    })
  })

  it('Should error when trying to update a disabled or a trunk port', async () => {
    ssh.updatePort.mockRejectedValue()

    // Disabled port
    await service.update(device.id, device.ports[2].id, device.vlans[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Cannot update trunks or disabled ports'))
    })

    // Trunk port
    await service.update(device.id, device.ports[1].id, device.vlans[1].id, user).catch((err) => {
      expect(err).toEqual(new Error('Cannot update trunks or disabled ports'))
    })
  })
  /* END UPDATE PORT */
})
