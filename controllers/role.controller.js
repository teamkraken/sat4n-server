const service = require('../services/role.service')
const { hasAccess } = require('../utils')

module.exports = {
  list: async (req, res, next) => {
    return await hasAccess('roles_list', req.user.role.id)
      .then(async () => {
        return await service.list()
          .then((roles) => { return res.status(200).json(roles) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  get: async (req, res, next) => {
    return await hasAccess('roles_get', req.user.role.id)
      .then(async () => {
        return await service.get(req.params.id)
          .then((role) => { return res.status(200).json(role) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  create: async (req, res, next) => {
    return await hasAccess('roles_create', req.user.role.id)
      .then(async () => {
        return await service.create(req.body, req.user.user)
          .then((role) => { return res.status(201).json(role) })
          .catch((err) => { return res.status(412).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  update: async (req, res, next) => {
    return await hasAccess('roles_update', req.user.role.id)
      .then(async () => {
        return await service.update(req.body, req.params.id, req.user.user)
          .then((role) => { return res.status(200).json(role) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  delete: async (req, res, next) => {
    return await hasAccess('roles_delete', req.user.role.id)
      .then(async () => {
        return await service.delete(req.params.id, req.user.user)
          .then((success) => { return res.status(200).json(success) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  }
}
