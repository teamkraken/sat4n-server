const service = require('../services/device.service')
const { hasAccess } = require('../utils')

module.exports = {
  list: async (req, res, next) => {
    return await hasAccess('devices_list', req.user.role.id)
      .then(async () => {
        return await service.list()
          .then((devices) => { return res.status(200).json(devices) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  get: async (req, res, next) => {
    return await hasAccess('devices_get', req.user.role.id)
      .then(async () => {
        return await service.get(req.params.id)
          .then((device) => { return res.status(200).json(device) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  create: async (req, res, next) => {
    const { ip } = req.body

    if (ip === '') {
      return res.status(400).json(new Error('Bad Request'))
    }

    return await hasAccess('devices_create', req.user.role.id)
      .then(async () => {
        return await service.create(req.body.ip, req.user.user)
          .then((device) => { return res.status(201).json(device) })
          .catch((err) => { return res.status(412).json(err.message) })
      })
      .catch((err) => { return res.status(403).json(err.message) })
  },

  delete: async (req, res, next) => {
    const { id } = req.params

    if (isNaN(id)) {
      return res.status(400).json(new Error('Bad Request'))
    }

    return await hasAccess('devices_delete', req.user.role.id)
      .then(async () => {
        return await service.delete(req.params.id, req.user.user)
          .then((success) => { return res.status(200).json(success) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  }
}
