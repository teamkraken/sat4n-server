/* eslint-env jest */
const utils = require('./index')

describe('Utils', () => {
/*   const user = {
    id: 1,
    username: 'admin'
  } */
  it('Should resolve when user has access', async () => {
    const access = await utils.hasAccess('devices_get', 1)

    expect(access).toBe(true)
  })

  it('Should fail if user does not have access', async () => {
    expect(utils.hasAccess('audit_get', 2)).rejects.toThrow(new Error('You are not authorized to view this resource'))
  })

  it('Should fail if role does not exist', async () => {
    expect(utils.hasAccess('devices_get', 2222)).rejects.toThrow(new Error("Cannot read properties of null (reading 'resources')"))
  })
})
