'use strict'
module.exports = (sequelize, DataTypes) => {
  const Vlan = sequelize.define('vlan', {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false
    },

    name: {
      type: DataTypes.STRING
    }
  }, {})

  Vlan.associate = function (models) {
    Vlan.belongsToMany(models.device, {
      through: 'deviceVlan',
      as: 'devices'
    })
  }

  return Vlan
}
