const service = require('../services/port.service')
const { hasAccess } = require('../utils')

module.exports = {
  update: async (req, res, next) => {
    if (isNaN(req.body.vlan)) {
      return res.status(400).json('Bad Request')
    }

    return await hasAccess('ports_update', req.user.role.id)
      .then(async () => {
        return await service.update(req.params.deviceID, req.params.portID, req.body.vlan, req.user.user)
          .then((d) => { return res.status(200).json(d) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  restart: async (req, res, next) => {
    return await hasAccess('ports_restart', req.user.role.id)
      .then(async () => {
        return await service.restart(req.params.deviceID, req.params.portID, req.user.user)
          .then((d) => { return res.status(200).json(d) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  disable: async (req, res, next) => {
    return await hasAccess('ports_disable', req.user.role.id)
      .then(async () => {
        return await service.disable(req.params.deviceID, req.params.portID, req.user.user)
          .then((d) => { return res.status(200).json(d) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  },

  enable: async (req, res, next) => {
    return await hasAccess('ports_enable', req.user.role.id)
      .then(async () => {
        return await service.enable(req.params.deviceID, req.params.portID, req.user.user)
          .then((d) => { return res.status(200).json(d) })
          .catch((err) => { return res.status(404).json(err) })
      })
      .catch((err) => { return res.status(403).json(err) })
  }
}
