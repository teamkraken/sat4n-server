/* eslint-env jest */
const service = require('./device.service')
const ssh = require('./ssh.service')

jest.mock('./ssh.service')

const mockDevice = {
  interfaces: [
    {
      interface: 'GigabitEthernet0/1',
      status: 'up',
      lineProtocol: 'up (connected)',
      hardware: 'Gigabit Ethernet',
      mtu: '1500',
      bw: '1000000',
      dly: '10',
      reliability: '255/255',
      txload: '1/255',
      rxload: '1/255',
      output: '00:00:00',
      lastInput: '00:01:57',
      inputRate: '0',
      'packets/sec': '2',
      outputRate: '1000',
      packetsInput: '202522',
      bytes: '209117670',
      noBuffer: '0',
      received: '78338',
      runts: '0',
      giants: '0',
      throttles: '0',
      inputErrors: '0',
      crc: '0',
      frame: '0',
      overrun: '0',
      ignored: '0',
      watchdog: '0',
      multicast: '49530',
      pauseInput: '0',
      inputPacketsWithDribbleConditionDetected: '0',
      packetsOutput: '2357842',
      underruns: '0',
      outputErrors: '0',
      collisions: '0',
      interfaceResets: '1',
      babbles: '0',
      lateCollision: '0',
      deferred: '0',
      lostCarrier: '0',
      noCarrier: '0',
      PAUSEOutput: '0',
      outputBufferFailures: '0',
      outputBuffersSwappedOut: '0'
    }
  ],
  interfacesStatus: [
    {
      name: 'Gi0/1',
      description: '',
      status: 'connected',
      vlan: 'trunk',
      duplex: 'a-full',
      speed: 'a-1000',
      type: '10/100/1000BaseTX'
    }
  ],
  vlans: [{ vlan: '666', name: 'Kraken', status: 'active', ports: '' }],
  neighbors: [
    {
      device: 'SEPD4AD717F047C',
      localPort: 'Fas 0/2',
      holdTime: '156',
      capability: 'H P M',
      platform: 'IP Phone',
      remotePort: 'Port 1'
    }
  ],
  macTable: [
    {
      vlan: '1',
      macAddress: '1A:2C:3A:4E:00:F2',
      type: 'DYNAMIC',
      ports: 'Gi0/1'
    },
    {
      vlan: '200',
      macAddress: 'D4:AD:71:7F:04:7C',
      type: 'STATIC',
      ports: 'Fa0/2'
    }
  ],
  ipSnoop: [
    {
      macAddress: '1A:2C:3A:4E:00:F2',
      ipAddress: '10.100.15.3',
      lease: '83578',
      type: 'dhcp-snooping',
      vlan: '1',
      interface: 'GigabitEthernet0/1'
    }
  ],
  arpTable: [
    {
      protocol: 'Internet',
      address: '10.100.15.1',
      age: '-',
      macAddress: '00:22:91:C6:3C:40',
      type: 'ARPA',
      interface: 'Vlan1'
    }
  ],
  info: {
    name: 'c3560-test-1',
    uptime: '1 week, 48 minutes',
    model: 'WS-C3560-8PC',
    os: 'Cisco IOS Software',
    osVersion: '12.2(55)SE4',
    osImage: 'C3560-IPSERVICESK9-M',
    portCount: '9',
    location: 'Eir5;2E;Nethopur'
  },
  log: [
    '*Mar  7 12:06:14.459: %DHCP_SNOOPING-4-NTP_NOT_RUNNING: NTP is not running; reloaded binding lease expiration times are incorrect.'
  ]
}

describe('Device Service', () => {
  const user = { id: 1, username: 'Administrator' }
  let deviceID

  it('Should return error when trying to create a device that is offline', async () => {
    ssh.getInfo.mockRejectedValue()

    await service.create('10.100.15.2', user).catch((err) => {
      expect(err).toEqual(new Error('Unable to establish connection to 10.100.15.2'))
    })
  })

  it('Should return created device', async () => {
    ssh.getInfo.mockResolvedValue(mockDevice)

    await service.create('10.100.15.2', user).then((d) => {
      deviceID = d.id

      expect(d.ip).toEqual('10.100.15.2')
    })
  })

  it('Should return error when trying to create a device with same IP', async () => {
    ssh.getInfo.mockRejectedValue()

    await service.create('10.100.15.2', user).catch((err) => {
      expect(err).toEqual(new Error('Device with this IP already exists'))
    })
  })

  it('Should return device by ID', async () => {
    ssh.getInfo.mockResolvedValue(mockDevice)

    await service.get(deviceID).then((d) => {
      expect(d.ip).toEqual('10.100.15.2')
    })
  })

  it('Should return error when device by ID is offline', async () => {
    ssh.getInfo.mockRejectedValue()

    await service.get(deviceID).catch((err) => {
      expect(err).toEqual(new Error('Unable to establish connection to 10.100.15.2'))
    })
  })

  it('Should list devices', async () => {
    await service.list().then((devices) => {
      expect(devices.filter((d) => d.id === deviceID)[0].ip).toEqual('10.100.15.2')
    })
  })

  it('Should delete device by ID', async () => {
    await service.delete(deviceID, user).then((d) => {
      expect(d).toBe(1)
    })
  })

  it('Should fail when same device is deleted again', async () => {
    await service.delete(deviceID, user).catch((err) => {
      expect(err).toEqual(new Error('Device not found'))
    })
  })

  it('Should fail when we get the deleted device', async () => {
    await service.get(deviceID).catch((err) => {
      expect(err).toEqual(new Error('Device not found'))
    })
  })
})
