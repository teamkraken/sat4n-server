/* globals test */
const request = require('supertest')
const app = require('../app')

test('Should throw a bad request error', (done) => {
  request(app)
    .post('/auth')
    .expect(400)
    .end((err) => {
      if (err) throw done(err)
      done()
    })
})
