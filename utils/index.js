const { role, apiResource } = require('../models')
const Op = require('sequelize').Op

module.exports = {
  hasAccess: async (resource, roleId) => {
    return await role.findOne({
      where: { id: { [Op.eq]: roleId } },
      include: [{
        model: apiResource,
        as: 'resources',
        through: { attributes: [] },
        attributes: ['id', 'name']
      }],
      attributes: ['id', 'name']
    })
      .then((d) => {
        if (d.resources.map(r => r.name).filter(d => d === resource).length === 1) {
          return true
        } else {
          throw new Error('You are not authorized to view this resource')
        }
      })
      .catch((err) => { throw err })
  }
}
