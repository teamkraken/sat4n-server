const Op = require('sequelize').Op
const { audit } = require('../models')

module.exports = {
  list: async () => {
    return await audit.findAll({
      attributes: {
        exclude: ['updatedAt']
      },
      order: [['createdAt', 'DESC']]
    })
      .catch(/* istanbul ignore next */ (err) => { return err })
  },

  getByUserId: async (id) => {
    return await audit.findAll({
      where: { userId: { [Op.eq]: id } },
      attributes: {
        exclude: ['updatedAt']
      },
      order: [['createdAt', 'DESC']]
    })
      .then((logs) => { return logs })
      .catch(/* istanbul ignore next */(err) => { return err })
  },

  getByDeviceId: async (id) => {
    return await audit.findAll({
      where: { deviceId: { [Op.eq]: id } },
      attributes: {
        exclude: ['updatedAt']
      },
      order: [['createdAt', 'DESC']]
    })
      .then((logs) => { return logs })
      .catch(/* istanbul ignore next */(err) => { return err })
  }
}
