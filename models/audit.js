module.exports = (sequelize, DataTypes) => {
  const Audit = sequelize.define('audit', {
    action: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },

    details: {
      type: DataTypes.STRING,
      unique: false,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  })

  Audit.associate = function (models) {
    Audit.belongsTo(models.user, {
      as: 'user',
      allowNull: false
    })

    Audit.belongsTo(models.device, {
      as: 'device',
      allowNull: true
    })
  }

  return Audit
}
