'use strict'
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('role', {
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        notEmpty: true
      }
    },

    description: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ''
    }
  }, {})

  Role.associate = function (models) {
    Role.belongsToMany(models.apiResource, {
      through: 'roleApiResource',
      as: 'resources'
    })

    Role.hasMany(models.user, {
      as: 'users'
    })
  }

  return Role
}
