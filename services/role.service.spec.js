/* eslint-env jest */
const service = require('./role.service')

describe('Role Service', () => {
  const user = {
    id: 1,
    username: 'admin'
  }

  it('Should return the new role', async () => {
    const role = await service.create({
      name: 'TestRole'
    }, user)

    // The user should be user
    expect(role.name).toBe('TestRole')
  })

  it('Should return error when trying to add an existing role', async () => {
    const role = service.create({
      name: 'TestRole'
    }, user)

    // The user should be user
    await expect(role).rejects.toThrow(new Error('Role with this name already exists'))
  })

  it('Should return a list of roles', async () => {
    const roles = await service.list()

    // Get a list of roles
    expect(roles.length).toBeGreaterThan(0)

    // The first user should be admin
    expect(roles[0].name).toBe('Administrators')
  })

  it('Should return role with ID of 1', async () => {
    const role = await service.get(1)

    // The first user should be admin
    expect(role.name).toBe('Administrators')
  })

  it('Should return the updated role', async () => {
    const roles = await service.list()
    // const role = roles.filter((u) => u.name === 'TestRole')
    const role = await service.get(roles.filter((u) => u.name === 'TestRole')[0].id)
    // expect(role.length).toBe(1)

    const newRole = await service.update({
      id: role.id,
      name: role.name,
      resources: role.resources
    }, role.id, user)

    // The user should be user
    expect(newRole.name).toBe('TestRole')
  })

  it('Should return the ID of deleted role', async () => {
    const roles = await service.list()
    const role = roles.filter((u) => u.name === 'TestRole')
    expect(role.length).toBe(1)

    const success = await service.delete(role[0].id, user)

    // The user should be user
    expect(success).toBe(1)
  })
})
