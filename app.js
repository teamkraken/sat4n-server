const path = require('path')
const bodyParser = require('body-parser')
const cors = require('cors')
const express = require('express')
const passport = require('passport')
const winston = require('winston')
const expressWinston = require('express-winston')
const { apiRouter, authRouter, docsRouter } = require('./routes')
require('./config/passport')

const app = express()

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console()
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.simple()
    // winston.format.json()
  ),
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: 'HTTP {{req.method}} {{req.url}}', // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: function (req, res) { return false } // optional: allows to skip some log messages based on request and/or response
}))

app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(passport.initialize())

app.use('/api', apiRouter)
app.use('/auth', authRouter)
app.use('/docs', docsRouter)

// Serving static files from public
app.use('/', express.static(path.join(__dirname, 'public')))
// Supplying routes for react app
app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'index.html')))

module.exports = app
