'use strict'
module.exports = (sequelize, DataTypes) => {
  const Device = sequelize.define('device', {
    name: {
      type: DataTypes.STRING
    },

    ip: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },

    uptime: {
      type: DataTypes.STRING
    },

    model: {
      type: DataTypes.STRING
    },

    os: {
      type: DataTypes.STRING
    },

    osVersion: {
      type: DataTypes.STRING
    },

    osImage: {
      type: DataTypes.STRING
    },

    portCount: {
      type: DataTypes.INTEGER
    },

    location: {
      type: DataTypes.STRING
    }
  }, {})

  Device.associate = function (models) {
    Device.belongsToMany(models.vlan, {
      through: 'deviceVlan',
      as: 'vlans'
    })

    Device.hasMany(models.port, {
      as: 'ports'
    })
  }

  return Device
}
