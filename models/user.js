module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('user', {
    username: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },

    enabled: {
      type: DataTypes.BOOLEAN,
      defaultValue: true
    }
  })

  User.associate = function (models) {
    User.hasMany(models.userSecret)

    User.belongsTo(models.role)
  }

  return User
}
