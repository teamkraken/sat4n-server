const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const Connection = require('ssh2').Client
const config = yaml.load(fs.readFileSync(path.join(__dirname, '../config/config.yaml')), 'utf8').cisco
const parser = require('../utils/parsers/cisco.ios.parser')

const sshConnect = (host, commands) => {
  return new Promise((resolve, reject) => {
    const c = new Connection()
    const finalResponse = {}

    c.on('ready', async function () {
      // If we got here we have a connection
      // Start by creating a shell
      c.shell(onShell)
    })

    const onShell = function (err, stream) {
      const totalCommands = commands.length
      let lastCommand = ''
      let ret = ''

      if (err != null) {
        console.log('error: ' + err)
      }

      let cmdcnt = 0
      stream.on('data', async function (data, extended) {
        const strData = String(data)

        // Check if we can input command or if we are receiving data
        if (strData.slice(-1) === '#') {
          ret += strData
          if (lastCommand !== '') {
            await onCommandComplete(lastCommand, ret)
          }
          ret = ''

          // Execute command
          if (commands.length > 0) {
            const command = `${commands.shift()}`
            lastCommand = command

            stream.write(command + '\n')
          }

          // Close connections if all commands have run
          if (cmdcnt === totalCommands) {
            stream.write('exit\n')
            c.end()
            resolve(finalResponse)
          }

          cmdcnt++
        } else { // Receiving data
          ret += strData
        }
      })
    }

    c.on('error', function (err) {
      reject(err)
    })

    c.on('close', function (hadError) {
      resolve(c.response)
    })

    c.connect({
      host,
      port: 22,
      username: config.username,
      password: config.password,
      algorithms: {
        kex: [
          'diffie-hellman-group1-sha1',
          'ecdh-sha2-nistp256',
          'ecdh-sha2-nistp384',
          'ecdh-sha2-nistp521',
          'diffie-hellman-group-exchange-sha256',
          'diffie-hellman-group14-sha1'],
        cipher: [
          'aes128-ctr',
          'aes192-ctr',
          'aes256-ctr',
          'aes128-gcm',
          'aes128-gcm@openssh.com',
          'aes256-gcm',
          'aes256-gcm@openssh.com',
          'aes256-cbc'],
        hmac: [
          'hmac-sha2-256',
          'hmac-sha2-512',
          'hmac-sha1'
        ]
      }
    })

    const onCommandComplete = async (command, response, sshObj) => {
      switch (command) {
        case 'show interfaces':
          finalResponse.interfaces = await parser.parse({ command, response })
          break
        case 'show interfaces status':
          finalResponse.interfacesStatus = await parser.parse({ command, response })
          break
        case 'show vlan brief':
          finalResponse.vlans = await parser.parse({ command, response })
          break
        case 'show cdp neighbors':
          finalResponse.neighbors = await parser.parse({ command, response })
          break
        case 'show mac address-table | exclude CPU':
          finalResponse.macTable = await parser.parse({ command, response })
          break
        case 'show ip dhcp snooping binding':
          finalResponse.ipSnoop = await parser.parse({ command, response })
          break
        case 'show ip arp':
          finalResponse.arpTable = await parser.parse({ command, response })
          break
        case 'show version':
          finalResponse.info = await parser.parse({ command, response })
          break
        case 'show snmp location':
          finalResponse.info = { ...finalResponse.info, location: await parser.parse({ command, response })[0] }
          break
        case 'show log':
          finalResponse.log = await parser.parse({ command, response })
          break
      }

      return finalResponse
    }
  })
}

module.exports = {
  getInfo: async (host, type) => {
    return new Promise((resolve, reject) => {
      const commands = [
        'terminal length 0',
        'show interfaces',
        'show interfaces status',
        'show vlan brief',
        'show cdp neighbors',
        'show mac address-table | exclude CPU',
        'show ip dhcp snooping binding',
        'show ip arp',
        'show version',
        'show snmp location',
        'show log'
      ]

      sshConnect(host, commands)
        .then((data) => { resolve(data) })
        .catch((err) => { reject(err) })
    })
  },

  updatePort: (host, port, vlan) => {
    return new Promise((resolve, reject) => {
      const commands = [
        'configure terminal',
        `interface ${port}`,
        `switchport access vlan ${vlan}`,
        'end',
        'terminal length 0',
        'show interfaces',
        'show interfaces status',
        'show vlan brief',
        'show cdp neighbors',
        'show mac address-table | exclude CPU',
        'show ip dhcp snooping binding',
        'show ip arp',
        'show version',
        'show snmp location',
        'show log',
        'wr'
      ]

      sshConnect(host, commands)
        .then((data) => { resolve(data) })
        .catch((err) => { reject(err) })
    })
  },

  restartPort: (host, port) => {
    return new Promise((resolve, reject) => {
      const commands = [
        'configure terminal',
        `interface ${port}`,
        'shutdown',
        'no shutdown',
        'end'
      ]

      sshConnect(host, commands)
        .then((data) => { resolve(data) })
        .catch((err) => { reject(err) })
    })
  },

  disablePort: (host, port) => {
    return new Promise((resolve, reject) => {
      const commands = [
        'configure terminal',
        `interface ${port}`,
        'shutdown',
        'end',
        'terminal length 0',
        'show interfaces',
        'show interfaces status',
        'show vlan brief',
        'show cdp neighbors',
        'show mac address-table | exclude CPU',
        'show ip dhcp snooping binding',
        'show ip arp',
        'show version',
        'show snmp location',
        'show log',
        'wr'
      ]

      sshConnect(host, commands)
        .then((data) => { resolve(data) })
        .catch((err) => { reject(err) })
    })
  },

  enablePort: (host, port) => {
    return new Promise((resolve, reject) => {
      const commands = [
        'configure terminal',
        `interface ${port}`,
        'no shutdown',
        'end',
        'terminal length 0',
        'show interfaces',
        'show interfaces status',
        'show vlan brief',
        'show cdp neighbors',
        'show mac address-table | exclude CPU',
        'show ip dhcp snooping binding',
        'show ip arp',
        'show version',
        'show snmp location',
        'show log',
        'wr'
      ]

      sshConnect(host, commands)
        .then((data) => { resolve(data) })
        .catch((err) => { reject(err) })
    })
  }
}
