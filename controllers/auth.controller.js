const jwt = require('jsonwebtoken')
const fs = require('fs')
const path = require('path')
const yaml = require('js-yaml')
const config = yaml.load(fs.readFileSync(path.join(__dirname, '../config/config.yaml')), 'utf8')
const service = require('../services/auth.service')

module.exports = {
  localLogin: (username, password, done) => {
    service.localLogin({ username, password })
      .then((user) => { done(null, user) })
      .catch((err) => { done(null, false, err) })
  },

  jwtLogin: (jwtPayload, done) => {
    service.jwtLogin(jwtPayload)
      .then((payload) => { done(null, payload) })
      .catch((err) => { done(null, false, err) })
  },

  generateJWT: (req, res) => {
    const { user } = req
    const { role } = user

    // TODO: implement fully working payload
    const jwtPayload = {
      user: { id: user.id, username: user.username },
      role: { id: role.id, name: role.name },
      exp: Math.round(Date.now() / 1000) + 8 * 60 * 60
    }

    const userResources = user.role.resources.map((r) => { return r.name })
    const resources = {
      audit: [],
      devices: [],
      ports: [],
      roles: [],
      users: []
    }

    userResources.forEach(s => {
      const temp = s.split('_')
      resources[temp[0]].push(temp[1])
    })

    res.json({
      user: { id: user.id, username: user.username },
      resources,
      token: jwt.sign(jwtPayload, config.JWT_SECRET)
    })
  }
}
