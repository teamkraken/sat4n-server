/* eslint-env jest */
const controller = require('./auth.controller')

describe('Auth Controller', () => {
  it('Mock test', async () => {
    await controller.generateJWT({
      user: {
        id: 1,
        username: 'admin',
        role: {
          id: 1,
          name: 'Administrators',
          resources: [{ id: 1, name: 'users_add' }]
        }
      }
    }, {
      json: (d) => {
        expect(Object.keys(d)).toEqual(['user', 'resources', 'token'])
      }
    })
  })
})
