/* eslint-env jest */
// TODO: test getByDeviceId when we figure out how to mock ssh.service
const service = require('./audit.service')
const userService = require('./user.service')

describe('Audit Service', () => {
  let auditUser
  const user = {
    id: 1,
    username: 'admin'
  }

  beforeAll(async () => {
    auditUser = await userService.create({ username: 'newAuditServiceUser', password: '123', roleId: 1 }, user)
  })

  afterAll(() => {
    userService.delete(auditUser.id, user)
  })

  it('Should return a list of all audit logs', async () => {
    const audit = await service.list()
    expect(audit.length).toBeGreaterThan(0)
  })

  it('Should return audit log associated with a specific user', async () => {
    const audit = await service.getByUserId(1)
    expect(audit.length).toBeGreaterThan(0)
  })
})
