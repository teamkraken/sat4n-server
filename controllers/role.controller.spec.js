/* eslint-env jest */
const controller = require('./role.controller')

describe('Role Controller', () => {
  // Mock express res
  const res = {}
  res.json = (d) => { return d }
  res.send = (d) => { return d }
  res.status = () => res

  let role = {}

  it('Should create role controller_testrole', async () => {
    // Create user testrole
    role = await controller.create({
      body: { name: 'controller_testrole' },
      user: {
        user: { id: 1 },
        role: { id: 1 }
      }
    }, res, null)

    expect(role.name).toBe('controller_testrole')
  })

  it('Should get role controller_testrole created in earlier test', async () => {
    // Get testrole by id
    const preDelete = await controller.get({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: role.id }
    }, res, null)

    expect(preDelete.id).toBe(role.id)
  })

  it('Should find user controller_testrole in a list of roles', async () => {
    const roleList = await controller.list({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      }
    }, res, null)

    expect(roleList.length).toBeGreaterThan(0)

    expect(roleList.filter((r) => r.name === 'controller_testrole').length).toBe(1)
  })

  it('Should update role controller_testrole', async () => {
    const updatedRole = await controller.update({
      body: {
        ...JSON.parse(JSON.stringify(role)),
        name: 'super_controller_testrole',
        resources: []
      },
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: role.id }
    }, res, null)

    expect(updatedRole.name).toBe('super_controller_testrole')
  })

  it('Should delete role super_controller_testrole', async () => {
    // Delete super_controller_testrole by ID
    const deleteTest = await controller.delete({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: role.id }
    }, res, null)

    expect(deleteTest).toBe(1)
  })

  it('Should not find role super_controller_testrole after it has been deleted', async () => {
    const postDelete = await controller.get({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: role.id }
    }, res, null)

    expect(postDelete).toEqual(new Error('Role not found'))
  })

  it('Should not be able to create role', async () => {
    const test = await controller.create({
      body: {
        name: 'tester_role'
      },
      user: {
        role: { id: 2 }
      }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to list roles', async () => {
    const test = await controller.list({
      user: {
        role: { id: 2 }
      }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to get role', async () => {
    const test = await controller.get({
      user: {
        role: { id: 2 }
      },
      params: { id: role.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to delete role', async () => {
    const test = await controller.delete({
      user: {
        role: { id: 2 }
      },
      params: { id: role.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to update role', async () => {
    const test = await controller.update({
      body: {
        name: 'testar'
      },
      user: {
        role: { id: 2 }
      },
      params: { id: role.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })
})
