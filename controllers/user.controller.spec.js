/* eslint-env jest */
const controller = require('./user.controller')

describe('User Controller', () => {
  // Mock express res
  const res = {}
  res.json = (d) => { return d }
  res.send = (d) => { return d }
  res.status = () => res

  let user = {}

  it('Should get admin with ID 1', async () => {
    const test = await controller.get({ user: { role: { id: 1 } }, params: { id: 1 } }, res, null)

    expect(test.username).toBe('admin')
  })

  it('Should create user testertestson', async () => {
    // Create user testertestson
    user = await controller.create({
      body: { username: 'testertestson', password: 'test', roleId: 2 },
      user: {
        user: { id: 1, username: 'admin' },
        role: { id: 1 }
      }
    }, res, null)

    expect(user.username).toBe('testertestson')
  })

  it('Should get user testertestson created in earlier test', async () => {
    // Get testertestson by id
    const preDelete = await controller.get({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: user.id }
    }, res, null)

    expect(preDelete.id).toBe(user.id)
  })

  it('Should find user testertestson in a list of users', async () => {
    const userList = await controller.list({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      }
    }, res, null)

    expect(userList.length).toBeGreaterThan(0)

    expect(userList.filter((u) => u.username === 'testertestson').length).toBe(1)
  })

  it('Should update user testertestson', async () => {
    const updatedUser = await controller.update({
      body: {
        ...user,
        id: 1000,
        username: 'testson',
        roleId: 1
      },
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: user.id }
    }, res, null)

    expect(updatedUser.username).toBe('testson')
  })

  it('Should delete user testertestson', async () => {
    // Delete testertestson by ID
    const deleteTest = await controller.delete({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: user.id }
    }, res, null)

    expect(deleteTest).toBe(1)
  })

  it('Should not find user testertestson after he has been deleted', async () => {
    // Get testertestson by id
    const postDelete = await controller.get({
      user: {
        user: { id: 1 },
        role: { id: 1 }
      },
      params: { id: user.id }
    }, res, null)

    expect(postDelete).toEqual(new Error('User not found'))
  })

  it('Should not be able to create user', async () => {
    const test = await controller.create({
      body: {
        username: 'tester',
        password: 'test'
      },
      user: {
        role: { id: 2 }
      }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to list users', async () => {
    const test = await controller.list({
      user: {
        role: { id: 2 }
      }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to get user', async () => {
    const test = await controller.get({
      user: {
        role: { id: 2 }
      },
      params: { id: user.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to delete user', async () => {
    const test = await controller.delete({
      user: {
        role: { id: 2 }
      },
      params: { id: user.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should not be able to update user', async () => {
    const test = await controller.update({
      body: {
        username: 'testar'
      },
      user: {
        role: { id: 2 }
      },
      params: { id: user.id }
    }, res, null)

    expect(test).toEqual(new Error('You are not authorized to view this resource'))
  })
})
