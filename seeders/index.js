const fs = require('fs')
const path = require('path')
const Op = require('sequelize').Op

// Check and copy from example file if it doesn't exist
if (!fs.existsSync(path.join(__dirname, '../config/config.yaml'))) {
  fs.copyFileSync(path.join(__dirname, '../config/config.yaml.example'), path.join(__dirname, '../config/config.yaml'), 0)
}

const db = require('../models')

const { sequelize } = db

sequelize.sync({ force: true })
  .then(async () => {
    const adminRole = await db.role.create({ name: 'Administrators', description: '' })
    const adminUser = await db.user.create({ username: 'admin' })
    const adminPassword = await db.userSecret.create({ secret: 'admin' })

    const apiResource = await db.apiResource.bulkCreate([
      { name: 'audit_get', description: '' },
      { name: 'devices_create', description: '' },
      { name: 'devices_delete', description: '' },
      { name: 'devices_get', description: '' },
      { name: 'devices_list', description: '' },
      { name: 'ports_update', description: '' },
      { name: 'ports_restart', description: '' },
      { name: 'ports_disable', description: '' },
      { name: 'ports_enable', description: '' },
      { name: 'roles_create', description: '' },
      { name: 'roles_delete', description: '' },
      { name: 'roles_update', description: '' },
      { name: 'roles_get', description: '' },
      { name: 'roles_list', description: '' },
      { name: 'users_create', description: '' },
      { name: 'users_delete', description: '' },
      { name: 'users_update', description: '' },
      { name: 'users_get', description: '' },
      { name: 'users_list', description: '' }
    ], { returning: true })

    await adminRole.setResources(apiResource)
    await adminUser.setRole(adminRole)

    await adminPassword.setUser(adminUser)

    const userRole = await db.role.create({ name: 'Users', description: '' })

    const userResource = await db.apiResource.findAll({
      where: { name: { [Op.in]: ['devices_get', 'devices_list'] } }
    })

    await userRole.setResources(userResource)
  })
