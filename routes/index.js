const apiRouter = require('./api.router')
const authRouter = require('./auth.router')
const docsRouter = require('./docs.router')

module.exports = {
  apiRouter,
  authRouter,
  docsRouter
}
