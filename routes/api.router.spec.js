/* globals test */
const request = require('supertest')
const app = require('../app')

test('Should throw an unauthorized error', (done) => {
  request(app)
    .get('/api/devices')
    .expect(401)
    .end((err) => {
      if (err) throw done(err)
      done()
    })
})
