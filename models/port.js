'use strict'
module.exports = (sequelize, DataTypes) => {
  const Port = sequelize.define('port', {
    name: {
      type: DataTypes.STRING
    },

    status: {
      type: DataTypes.STRING
    },

    vlan: {
      type: DataTypes.STRING
    },

    duplex: {
      type: DataTypes.STRING
    },

    speed: {
      type: DataTypes.STRING
    },

    type: {
      type: DataTypes.STRING
    },

    lastUsed: {
      type: DataTypes.STRING
    },

    inputErrors: {
      type: DataTypes.INTEGER
    },

    outputErrors: {
      type: DataTypes.INTEGER
    }
  }, {})

  Port.associate = function (models) {
    Port.belongsTo(models.device)
  }

  return Port
}
