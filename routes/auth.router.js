const Router = require('express').Router
const passport = require('passport')
const { authController } = require('../controllers')
require('../config/passport')

const router = Router()

/**
 * @swagger
 *  /auth:
 *    post:
 *      tags:
 *        - auth
 *      description: Login to the application
 *      produces:
 *        - application/json
 *      requestBody:
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              type: object
 *              properties:
 *                username:
 *                  type: string
 *                password:
 *                  type: string
 *              required:
 *                - username
 *                - password
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                username:
 *                  type: string
 *                password:
 *                  type: string
 *              required:
 *                - username
 *                - password
 *      responses:
 *        200:
 *          description: login
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  user:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: integer
 *                      username:
 *                        type: string
 *                  resources:
 *                    type: object
 *                    properties:
 *                      audit:
 *                        type: array
 *                        items:
 *                          type: string
 *                      devices:
 *                        type: array
 *                        items:
 *                          type: string
 *                      ports:
 *                        type: array
 *                        items:
 *                          type: string
 *                      roles:
 *                        type: array
 *                        items:
 *                          type: string
 *                      users:
 *                        type: array
 *                        items:
 *                          type: string
 *                  token:
 *                    type: string
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
*/
router.post('/', passport.authenticate(['local'], { session: false }), authController.generateJWT)

module.exports = router
