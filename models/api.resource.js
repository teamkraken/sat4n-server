'use strict'
module.exports = (sequelize, DataTypes) => {
  const ApiResource = sequelize.define('apiResource', {
    name: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },

    description: {
      type: DataTypes.STRING,
      allowNull: false,
      defaultValue: ''
    }
  }, {})

  ApiResource.associate = (models) => {
    ApiResource.belongsToMany(models.role, {
      through: 'roleApiResource',
      as: 'roles'
    })
  }

  return ApiResource
}
