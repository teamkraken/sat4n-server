const Op = require('sequelize').Op
const { apiResource, role, user, userSecret } = require('../models')

module.exports = {
  localLogin: async (credentials) => {
    return await user.findOne({
      where: { username: { [Op.eq]: credentials.username } },
      attributes: ['id', 'username', 'roleId'],
      include: [
        {
          model: role,
          attributes: ['id', 'name'],
          include: [{
            model: apiResource,
            as: 'resources',
            attributes: ['id', 'name']
          }]
        },
        {
          model: userSecret,
          order: [['createdAt', 'DESC']],
          limit: 1
        }
      ]
    })
      .then(async (user) => {
        if (await userSecret.validPassword(credentials.password, user.userSecrets[0].secret)) {
          return {
            id: user.id,
            username: user.username,
            role: {
              id: user.role.id,
              name: user.role.name,
              resources: user.role.resources.map((r) => { return { id: r.id, name: r.name } })
            }
          }
        } else {
          throw new Error('Invalid username or password')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  jwtLogin: async (payload) => {
    return await user.findOne({
      where: {
        username: {
          [Op.eq]: payload.user.username
        }
      },
      attributes: ['username']
    })
      .then((user) => {
        if (user) {
          return payload
        } else {
          throw new Error('401: Invalid user')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  }
}
