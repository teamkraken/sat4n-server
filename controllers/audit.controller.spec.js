/* eslint-env jest */
const controller = require('./audit.controller')
const userService = require('../services/user.service')
const deviceService = require('../services/device.service')
const ssh = require('../services/ssh.service')

jest.mock('../services/ssh.service')

const mockDevice = {
  interfaces: [],
  interfacesStatus: [],
  vlans: [],
  neighbors: [],
  macTable: [],
  ipSnoop: [],
  arpTable: [],
  info: {
    name: 'c3560-test-1',
    uptime: '1 week, 48 minutes',
    model: 'WS-C3560-8PC',
    os: 'Cisco IOS Software',
    osVersion: '12.2(55)SE4',
    osImage: 'C3560-IPSERVICESK9-M',
    portCount: '9',
    location: 'Eir5;2E;Nethopur'
  },
  log: []
}

// TODO: Fix tests when we know how to test against devices
describe('Audit Controller', () => {
  const res = {}
  res.json = (d) => { return d }
  res.send = (d) => { return d }
  res.status = () => res

  let auditUser
  const user = {
    id: 1
  }

  beforeAll(async () => {
    auditUser = await userService.create({ username: 'newAuditControllerUser', password: '123', roleId: 2 }, user)
  })

  afterAll(async () => {
    await userService.delete(auditUser.id, user)
  })

  it('Should get a list of all audit logs', async () => {
    const test = await controller.list({ user: { role: { id: 1 } } }, res, null)

    expect(test.length).toBeGreaterThan(0)
  })

  it('Should get audit logs associated with userId 1', async () => {
    const test = await controller.getByUserId({ user: { role: { id: 1 } }, params: { id: 1 } }, res, null)

    expect(test.length).toBeGreaterThan(0)
  })

  it('Should get audit logs associated with deviceId 1', async () => {
    ssh.getInfo.mockResolvedValue(mockDevice)

    // Create device
    const device = await deviceService.create('0.0.0.2', user)

    // Check audit for device
    const test = await controller.getByDeviceId({ user: { role: { id: 1 } }, params: { id: device.id } }, res, null)

    // Delete device
    await deviceService.delete(device.id, user)

    expect(test.length).toBeGreaterThan(0)
  })

  it('Should fail when user does not have access (list)', async () => {
    expect(await controller.list({ user: { role: { id: 2 } } }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should fail when user does not have access (getByUserId)', async () => {
    expect(await controller.getByUserId({ user: { role: { id: 2 } }, params: { id: 1 } }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should fail when user does not have access (getByDeviceId)', async () => {
    expect(await controller.getByDeviceId({ user: { role: { id: 2 } }, params: { id: 1 } }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })
})
