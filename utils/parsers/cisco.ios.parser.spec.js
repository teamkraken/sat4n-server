/* eslint-env jest */
const parser = require('./cisco.ios.parser')

const interfaceResponse = `show interfaces\r
FastEthernet0/8 is up, line protocol is up (connected)\r
  Hardware is Fast Ethernet, address is 0022.91c6.3c09 (bia 0022.91c6.3c09)\r
  MTU 1500 bytes, BW 100000 Kbit, DLY 100 usec,\r
     reliability 255/255, txload 1/255, rxload 1/255\r
  Encapsulation ARPA, loopback not set\r
  Keepalive set (10 sec)\r
  Full-duplex, 100Mb/s, media type is 10/100BaseTX\r
  input flow-control is off, output flow-control is unsupported\r
  ARP type: ARPA, ARP Timeout 04:00:00\r
  Last input 00:15:21, output 00:00:00, output hang never\r
  Last clearing of "show interface" counters never\r
  Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0\r
  Queueing strategy: fifo\r
  Output queue: 0/40 (size/max)\r
  5 minute input rate 0 bits/sec, 0 packets/sec\r
  5 minute output rate 0 bits/sec, 0 packets/sec\r
     76346 packets input, 43082090 bytes, 0 no buffer\r
     Received 45452 broadcasts (29225 multicasts)\r
     0 runts, 0 giants, 0 throttles\r
     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored\r
     0 watchdog, 29225 multicast, 0 pause input\r
     0 input packets with dribble condition detected\r
     458580 packets output, 34912481 bytes, 0 underruns\r
     0 output errors, 0 collisions, 1 interface resets\r
     0 babbles, 0 late collision, 0 deferred\r
     0 lost carrier, 0 no carrier, 0 PAUSE output\r
     0 output buffer failures, 0 output buffers swapped out\r
GigabitEthernet0/1 is down, line protocol is down (notconnect)\r
  Hardware is Gigabit Ethernet, address is 0022.91c6.3c01 (bia 0022.91c6.3c01)\r
  MTU 1500 bytes, BW 10000 Kbit, DLY 1000 usec,\r
     reliability 255/255, txload 1/255, rxload 1/255\r
  Encapsulation ARPA, loopback not set\r
  Keepalive not set\r
  Auto-duplex, Auto-speed, link type is auto, media type is Not Present\r
  input flow-control is off, output flow-control is unsupported\r
  ARP type: ARPA, ARP Timeout 04:00:00\r
  Last input never, output never, output hang never\r
  Last clearing of "show interface" counters never\r
  Input queue: 0/75/0/0 (size/max/drops/flushes); Total output drops: 0\r
  Queueing strategy: fifo\r
  Output queue: 0/40 (size/max)\r
  5 minute input rate 0 bits/sec, 0 packets/sec\r
  5 minute output rate 0 bits/sec, 0 packets/sec\r
     0 packets input, 0 bytes, 0 no buffer\r
     Received 0 broadcasts (0 multicasts)\r
     0 runts, 0 giants, 0 throttles\r
     0 input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored\r
     0 watchdog, 0 multicast, 0 pause input\r
     0 input packets with dribble condition detected\r
     0 packets output, 0 bytes, 0 underruns\r
     0 output errors, 0 collisions, 1 interface resets\r
     0 babbles, 0 late collision, 0 deferred\r
     0 lost carrier, 0 no carrier, 0 PAUSE output\r
     0 output buffer failures, 0 output buffers swapped out\r
c3560-test-1#
`

const interfaceStatusResponse = `show interfaces status\r
\r
Port      Name               Status       Vlan       Duplex  Speed Type\r
Fa0/1                        notconnect   10           auto   auto 10/100BaseTX\r
Fa0/2                        notconnect   20           auto   auto 10/100BaseTX\r
Fa0/3                        notconnect   30           auto   auto 10/100BaseTX\r
Fa0/4                        notconnect   1            auto   auto 10/100BaseTX\r
Fa0/5                        notconnect   1            auto   auto 10/100BaseTX\r
Fa0/6                        notconnect   1            auto   auto 10/100BaseTX\r
Fa0/7                        disabled     1            auto   auto 10/100BaseTX\r
Fa0/8                        connected    1          a-full  a-100 10/100BaseTX\r
Gi0/1                        notconnect   1            auto   auto Not Present\r
c3560-test-1#`

const vlanBrief = `show vlan brief\r
\
VLAN Name                             Status    Ports\r
---- -------------------------------- --------- -------------------------------\r
1    default                          active    Fa0/4, Fa0/5, Fa0/6, Fa0/7\r
                                                Fa0/8, Gi0/1\r
10   Sales                            active    Fa0/1\r
20   Marketing                        active    Fa0/2\r
30   Booking                          active    Fa0/3\r
1002 fddi-default                     act/unsup\r
1003 token-ring-default               act/unsup\r
1004 fddinet-default                  act/unsup\r
1005 trnet-default                    act/unsup\r
c3560-test-1#
`

const cdpNeighbors = `show cdp neighbors\r
Capability Codes: R - Router, T - Trans Bridge, B - Source Route Bridge\r
                  S - Switch, H - Host, I - IGMP, r - Repeater, P - Phone,\r
                  D - Remote, C - CVTA, M - Two-port Mac Relay\r
\r
Device ID        Local Intrfce     Holdtme    Capability  Platform  Port ID\r
c2960x-test-1.kraken.local\r
                 Gig 0/9           152              S I   WS-C2960X Gig 1/0/48\r
c3560-test-1#`

const macAddresses = `show mac address-table static | exclude CPU\r
Mac Address Table\r
-------------------------------------------\r
\r
Vlan    Mac Address       Type        Ports\r
----    -----------       --------    -----\r
100    40a8.f0a9.279a    STATIC      Gi0/8\r
100    480f.cf49.0cd2    STATIC      Gi0/1\r
Total Mac Addresses for this criterion: 23\r
c3560-test-1#
`

const ipAddresses = `show ip dhcp snooping binding\r
MacAddress          IpAddress        Lease(sec)  Type           VLAN  Interface\r
------------------  ---------------  ----------  -------------  ----  --------------------\r
1A:2C:3A:4E:00:F2   10.100.15.3      77791       dhcp-snooping   1     FastEthernet0/8\r
Total number of bindings: 1\r
\r
c3560-test-1#`

const arpTable = `show ip arp\r
Protocol  Address          Age (min)  Hardware Addr   Type   Interface\r
Internet  10.100.15.1             -   0022.91c6.3c40  ARPA   Vlan1\r
Internet  10.100.15.3           126   1a2c.3a4e.00f2  ARPA   Vlan1\r
Internet  10.100.18.1             -   0022.91c6.3c43  ARPA   Vlan30\r
Internet  10.100.17.1             -   0022.91c6.3c42  ARPA   Vlan20\r
Internet  10.100.16.1             -   0022.91c6.3c41  ARPA   Vlan10\r
c3560-test-1#`

const log = `show log\r
Syslog logging: enabled (0 messages dropped, 0 messages rate-limited, 0 flushes, 0 overruns, xml disabled, filtering disabled)\r
\r
No Active Message Discriminator.\r
\r
\r
\r
No Inactive Message Discriminator.\r
\r
\r
    Console logging: level debugging, 121 messages logged, xml disabled,\r
                     filtering disabled\r
    Monitor logging: level debugging, 0 messages logged, xml disabled,\r
                     filtering disabled\r
    Buffer logging:  level debugging, 121 messages logged, xml disabled,\r
                     filtering disabled\r
    Exception Logging: size (4096 bytes)\r
    Count and timestamp logging messages: disabled\r
    File logging: disabled\r
    Persistent logging: disabled\r
\r
No active filter modules.\r
\r
    Trap logging: level informational, 121 message lines logged\r
\r
Log Buffer (4096 bytes):\r
 04:57:41.264: %LINEPROTO-5-UPDOWN: Line protocol on Interface Vlan1, changed state to up\r
*Mar  9 05:02:48.497: %DHCP_SNOOPING-4-NTP_NOT_RUNNING: NTP is not running; reloaded binding lease expiration times are incorrect.\r
c3560-test-1#`

describe('Cisco Parser', () => {
  it('Should parse data and return an array with interfaces', async () => {
    const interfaces = await parser.parse({ command: 'show interfaces', response: interfaceResponse })

    expect(interfaces.length).toBe(2)
  })

  it('Should parse data and return an array with interfaces', async () => {
    const interfaces = await parser.parse({ command: 'show interfaces status', response: interfaceStatusResponse })

    expect(interfaces.length).toBe(9)
  })

  it('Should parse data and return an array with vlans', async () => {
    const interfaces = await parser.parse({ command: 'show vlan brief', response: vlanBrief })

    expect(interfaces.length).toBe(4)
  })

  it('Should parse data and return an array with neighbors', async () => {
    const interfaces = await parser.parse({ command: 'show cdp neighbors', response: cdpNeighbors })

    expect(interfaces.length).toBe(1)
  })

  it('Should parse data and return an array with macaddresses', async () => {
    const interfaces = await parser.parse({ command: 'show mac address-table | exclude CPU', response: macAddresses })

    expect(interfaces.length).toBe(2)
  })

  it('Should parse data and return an array with ipaddresses', async () => {
    const interfaces = await parser.parse({ command: 'show ip dhcp snooping binding', response: ipAddresses })

    expect(interfaces.length).toBe(1)
  })

  it('Should parse data and return an array with arp addresses', async () => {
    const interfaces = await parser.parse({ command: 'show ip arp', response: arpTable })

    expect(interfaces.length).toBe(5)
  })

  it('Should parse data and return an array with log', async () => {
    const interfaces = await parser.parse({ command: 'show log', response: log })

    expect(interfaces.length).toBe(27)
  })
})
