[![pipeline](https://gitlab.com/teamkraken/sat4n-server/badges/master/pipeline.svg)](https://gitlab.com/teamkraken/sat4n-server/commits/master)
[![coverage](https://gitlab.com/teamkraken/sat4n-server/badges/master/coverage.svg)](https://gitlab.com/teamkraken/sat4n-server/commits/master)
[![license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

# Simple Administrative Tools for Networking
This is a web service to make the management of enterprise level networks more efficient. This is
done by moving small and repetitive tasks from the command line to a web service. This is the actual
web service with an API for that service. This app is meant to run internally on the network being
managed.

## Requirements
* [node.js](http://nodejs.org/) -- v10.x.x
    * [sqlite3](https://www.npmjs.com/package/sqlite3) v4.0.6 doesn't support versions newer than [node.js](http://nodejs.org/) -- v10.x.x

## Getting started
To install the web service, follow these steps:

1. Have the latest version of [node.js](https://nodejs.org/en/) installed.
  
2. Clone the repository:
`<local>/ $ git clone https://gitlab.com/teamkraken/sat4n-server/`

3. Run the install script in the root directory:
`<local>/sat4n-server/ $ npm i`

4. Then run the setup script:
`<local>/sat4n-server/ $ npm run setup`

5. Then run the client using node:
`<local>/sat4n-server/ $ npm start`

The API documentation uses [SWAGGER.](https://swagger.io/tools/open-source/getting-started/)
* See documentation at [http://localhost:8080/docs](http://localhost:8080/docs)

## (Optional) Install as a linux service
To install as as SystemD service do the following, example assumes SAT:4N was installed into /opt

### Create service user
```
sudo useradd -r sat4n
```

### Grant service user ownership of sat4n directory
```
sudo chown -R sat4n:sat4n /opt/sat4n-server
```

### Copy service file to SystemD
```
sudo cp /opt/sat4n-server/config/sat4n.service.example /etc/systemd/system/sat4n.service
sudo systemctl enable sat4n
sudo systemctl start sat4n
```

## License
[gpl-3.0](LICENSE)
You may copy, distribute and modify the software as long as you track changes/dates in source
files. Any modifications to or software including (via compiler) GPL-licensed code must also
be made available under the GPL along with build & install instructions.
This license provides NO warranty and limits the authors liability to the full extent of the law.
This license does not forbid commercial use.
