const { Router } = require('express')
const passport = require('passport')
const { auditController, deviceController, portController, roleController, userController } = require('../controllers')
require('../config/passport')

const router = Router()

/**
 * @swagger
 *
 * tags:
 *  - name: auth
 *  - name: audit
 *  - name: devices
 *  - name: ports
 *  - name: roles
 *  - name: users
 * components:
 *  schemas:
 *    ArpAddress:
 *      type: object
 *      properties:
 *        protocol:
 *          type: string
 *        address:
 *          type: string
 *        age:
 *          type: string
 *        macAddress:
 *          type: string
 *        type:
 *          type: string
 *        interface:
 *          type: string
 *    ArpAddresses:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/ArpAddress'
 *    Audit:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        timestamp:
 *          type: string
 *        action:
 *          type: string
 *        details:
 *          type: string
 *    Audits:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Audit'
 *    Device:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        ip:
 *          type: string
 *        uptime:
 *          type: string
 *        model:
 *          type: string
 *        os:
 *          type: string
 *        osVersion:
 *          type: string
 *        osImage:
 *          type: string
 *        portCount:
 *          type: integer
 *        location:
 *          type: string
 *      required:
 *        - ip
 *    Devices:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Device'
 *    ExtendedDevice:
 *      allOf:
 *        - $ref: '#/components/schemas/Device'
 *      properties:
 *        arpTable:
 *          $ref: '#/components/schemas/ArpAddresses'
 *        ipSnoop:
 *          $ref: '#/components/schemas/SnoopAddresses'
 *        log:
 *          $ref: '#/components/schemas/Log'
 *        macTable:
 *          $ref: '#/components/schemas/MacAddresses'
 *        neighbors:
 *          $ref: '#/components/schemas/Neighbors'
 *        ports:
 *          $ref: '#/components/schemas/Ports'
 *        vlans:
 *          $ref: '#/components/schemas/Vlans'
 *    Log:
 *      type: array
 *      items:
 *        type: string
 *    MacAddress:
 *      type: object
 *      properties:
 *        vlan:
 *          type: string
 *        macAddress:
 *          type: string
 *        type:
 *          type: string
 *        ports:
 *          type: string
 *    MacAddresses:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/MacAddress'
 *    Neighbor:
 *      type: object
 *      properties:
 *        device:
 *          type: string
 *        localPort:
 *          type: string
 *        holdTime:
 *          type: string
 *        capability:
 *          type: string
 *        platform:
 *          type: string
 *        remotePort:
 *          type: string
 *    Neighbors:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Neighbor'
 *    Port:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        status:
 *          type: string
 *        vlan:
 *          type: string
 *        duplex:
 *          type: string
 *        speed:
 *          type: string
 *        type:
 *          type: string
 *        lastUsed:
 *          type: string
 *        inputErrors:
 *          type: string
 *        outputErrors:
 *          type: string
 *    Ports:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Port'
 *    Resource:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        enabled:
 *          type: boolean
 *    Resources:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Resource'
 *    Role:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *        description:
 *          type: string
 *      required:
 *        - name
 *    Roles:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Role'
 *    ExtendedRole:
 *      allOf:
 *        - $ref: '#/components/schemas/Role'
 *      properties:
 *        resources:
 *          $ref: '#/components/schemas/Resources'
 *    SnoopAddress:
 *      type: object
 *      properties:
 *        macAddress:
 *          type: string
 *        ipAddress:
 *          type: string
 *        lease:
 *          type: string
 *        type:
 *          type: string
 *        vlan:
 *          type: string
 *        interface:
 *          type: string
 *    SnoopAddresses:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/SnoopAddress'
 *    User:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        username:
 *          type: string
 *        role:
 *          $ref: '#/components/schemas/Role'
 *      required:
 *        - username
 *    Users:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/User'
 *    ExtendedUser:
 *      allOf:
 *        - $ref: '#/components/schemas/User'
 *      properties:
 *        password:
 *          type: string
 *        roleId:
 *          type: integer
 *      required:
 *        - password
 *    Vlan:
 *      type: object
 *      properties:
 *        id:
 *          type: integer
 *        name:
 *          type: string
 *    Vlans:
 *      type: array
 *      items:
 *        $ref: '#/components/schemas/Vlan'
 */

/**
 * @swagger
 *  /api/devices:
 *    get:
 *      tags:
 *        - devices
 *      summary: Get a list of devices
 *      description: Returns a list of devices
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Devices'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices', passport.authenticate(['jwt'], { session: false }), deviceController.list)

/**
 * @swagger
 *  /api/devices/{id}:
 *    get:
 *      tags:
 *        - devices
 *      summary: Get device by ID
 *      description: Returns a single device
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of device to return
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedDevice'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Device not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices/:id', passport.authenticate(['jwt'], { session: false }), deviceController.get)

/**
 * @swagger
 *  /api/devices:
 *    post:
 *      tags:
 *        - devices
 *      summary: Add device
 *      description: Returns a single device
 *      requestBody:
 *        required: true
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              type: object
 *              properties:
 *                ip:
 *                  type: string
 *              required:
 *                - ip
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                ip:
 *                  type: string
 *              required:
 *                - ip
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Device'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '412':
 *          description: Precondition Failed
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unable to establish connection to IP or Device with this IP already exists
 *      security:
 *        - bearerAuth: []
 */
router.post('/devices/', passport.authenticate(['jwt'], { session: false }), deviceController.create)

/**
 * @swagger
 *  /api/devices/{id}:
 *    delete:
 *      tags:
 *        - devices
 *      summary: Delete device by ID
 *      description: Returns 1 on success
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of device to delete
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: integer
 *              example: 1
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Device not found
 *      security:
 *        - bearerAuth: []
 */
router.delete('/devices/:id', passport.authenticate(['jwt'], { session: false }), deviceController.delete)

/**
 * @swagger
 *  /api/devices/{deviceID}/ports/{portID}:
 *    put:
 *      tags:
 *        - ports
 *      summary: Update port by deviceID and portID
 *      description: Returns Device
 *      parameters:
 *        - name: deviceID
 *          in: path
 *          description: ID of device to return
 *          required: true
 *          schema:
 *            type: integer
 *        - name: portID
 *          in: path
 *          description: ID of port to update
 *          required: true
 *          schema:
 *            type: integer
 *      requestBody:
 *        required: true
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              type: object
 *              properties:
 *                vlan:
 *                  type: integer
 *              required:
 *                - vlan
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                vlan:
 *                  type: integer
 *              required:
 *                - vlan
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedDevice'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Port not found
 *      security:
 *        - bearerAuth: []
 */
router.put('/devices/:deviceID/ports/:portID', passport.authenticate(['jwt'], { session: false }), portController.update)

/**
 * @swagger
 *  /api/devices/{deviceID}/ports/{portID}/restart:
 *    get:
 *      tags:
 *        - ports
 *      summary: Restarts port by deviceID and portID
 *      description: Returns Device
 *      parameters:
 *        - name: deviceID
 *          in: path
 *          description: ID of device to return
 *          required: true
 *          schema:
 *            type: integer
 *        - name: portID
 *          in: path
 *          description: ID of port to update
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: integer
 *              example: 1
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Port not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices/:deviceID/ports/:portID/restart', passport.authenticate(['jwt'], { session: false }), portController.restart)

/**
 * @swagger
 *  /api/devices/{deviceID}/ports/{portID}/disable:
 *    get:
 *      tags:
 *        - ports
*      summary: Disable port by deviceID and portID
 *      description: Returns Device
 *      parameters:
 *        - name: deviceID
 *          in: path
 *          description: ID of device to return
 *          required: true
 *          schema:
 *            type: integer
 *        - name: portID
 *          in: path
 *          description: ID of port to update
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedDevice'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Port not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices/:deviceID/ports/:portID/disable', passport.authenticate(['jwt'], { session: false }), portController.disable)

/**
 * @swagger
 *  /api/devices/{deviceID}/ports/{portID}/enable:
 *    get:
 *      tags:
 *        - ports
*      summary: Enable port by deviceID and portID
 *      description: Returns Device
 *      parameters:
 *        - name: deviceID
 *          in: path
 *          description: ID of device to return
 *          required: true
 *          schema:
 *            type: integer
 *        - name: portID
 *          in: path
 *          description: ID of port to update
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedDevice'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Port not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices/:deviceID/ports/:portID/enable', passport.authenticate(['jwt'], { session: false }), portController.enable)

/**
 * @swagger
 *  /api/roles:
 *    get:
 *      tags:
 *        - roles
 *      summary: Get a list of roles
 *      description: Returns a list of roles
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Roles'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *      security:
 *        - bearerAuth: []
 */
router.get('/roles', passport.authenticate(['jwt'], { session: false }), roleController.list)

/**
 * @swagger
 *  /api/roles/{id}:
 *    get:
 *      tags:
 *        - roles
 *      summary: Get role by ID
 *      description: Returns a single role
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of role to return
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedRole'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Role not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/roles/:id', passport.authenticate(['jwt'], { session: false }), roleController.get)

/**
 * @swagger
 *  /api/roles:
 *    post:
 *      tags:
 *        - roles
 *      summary: Add role
 *      description: Returns a single role
 *      requestBody:
 *        required: true
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              $ref: '#/components/schemas/Role'
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Role'
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedRole'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Role not found
 *        '412':
 *          description: Precondition Failed
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Role with this name already exists
 *      security:
 *        - bearerAuth: []
 */
router.post('/roles/', passport.authenticate(['jwt'], { session: false }), roleController.create)

/**
 * @swagger
 *  /api/roles/{id}:
 *    put:
 *      tags:
 *        - roles
 *      summary: Update role by ID
 *      description: Returns a single role
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of role to return
 *          required: true
 *          schema:
 *            type: integer
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedRole'
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Role'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Role not found
 *      security:
 *        - bearerAuth: []
 */
router.put('/roles/:id', passport.authenticate(['jwt'], { session: false }), roleController.update)

/**
 * @swagger
 *  /api/roles/{id}:
 *    delete:
 *      tags:
 *        - roles
 *      summary: Delete role by ID
 *      description: Returns 1 on success
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of role to delete
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: integer
 *              example: 1
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Role not found
 *      security:
 *        - bearerAuth: []
 */
router.delete('/roles/:id', passport.authenticate(['jwt'], { session: false }), roleController.delete)

/**
 * @swagger
 *  /api/users:
 *    get:
 *      tags:
 *        - users
 *      summary: Get a list of users
 *      description: Returns a list of users
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Users'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *      security:
 *        - bearerAuth: []
 */
router.get('/users', passport.authenticate(['jwt'], { session: false }), userController.list)

/**
 * @swagger
 *  /api/users/{id}:
 *    get:
 *      tags:
 *        - users
 *      summary: Get user by ID
 *      description: Returns a single user
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of user to return
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: User not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/users/:id', passport.authenticate(['jwt'], { session: false }), userController.get)

/**
 * @swagger
 *  /api/users:
 *    post:
 *      tags:
 *        - users
 *      summary: Add user
 *      description: Returns a single user
 *      requestBody:
 *        required: true
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUser'
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUser'
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/User'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: User not found
 *        '412':
 *          description: Precondition Failed
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: User with this username already exists
 *      security:
 *        - bearerAuth: []
 */
router.post('/users/', passport.authenticate(['jwt'], { session: false }), userController.create)

/**
 * @swagger
 *  /api/users/{id}:
 *    put:
 *      tags:
 *        - users
 *      summary: Update user by ID
 *      description: Returns a single user
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of user to return
 *          required: true
 *          schema:
 *            type: integer
 *      requestBody:
 *        required: true
 *        content:
 *          application/x-www-form-urlencoded:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUser'
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/ExtendedUser'
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/ExtendedUser'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Port not found
 *      security:
 *        - bearerAuth: []
 */
router.put('/users/:id', passport.authenticate(['jwt'], { session: false }), userController.update)

/**
 * @swagger
 *  /api/users/{id}:
 *    delete:
 *      tags:
 *        - users
 *      summary: Delete user by ID
 *      description: Returns 1 on success
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of user to delete
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                type: integer
 *              example: 1
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Not found
 *      security:
 *        - bearerAuth: []
 */
router.delete('/users/:id', passport.authenticate(['jwt'], { session: false }), userController.delete)

/**
 * @swagger
 *  /api/audit:
 *    get:
 *      tags:
 *        - audit
 *      summary: Get a global audit log
 *      description: Returns a global audit log
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Audits'
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *      security:
 *        - bearerAuth: []
 */
router.get('/audit', passport.authenticate(['jwt'], { session: false }), auditController.list)

/**
 * @swagger
 *  /api/users/{id}/audit:
 *    get:
 *      tags:
 *        - audit
 *      summary: Get audit log by user ID
 *      description: Returns audit log for user
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of user
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Audits'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: User not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/users/:id/audit', passport.authenticate(['jwt'], { session: false }), auditController.getByUserId)

/**
 * @swagger
 *  /api/device/{id}/audit:
 *    get:
 *      tags:
 *        - audit
 *      summary: Get audit log by device ID
 *      description: Returns audit log for device
 *      parameters:
 *        - name: id
 *          in: path
 *          description: ID of device
 *          required: true
 *          schema:
 *            type: integer
 *      responses:
 *        '200':
 *          description: successful operation
 *          content:
 *            application/json:
 *              schema:
 *                $ref: '#/components/schemas/Audits'
 *        '400':
 *          description: Bad Request
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Bad Request
 *        '401':
 *          description: Unauthorized
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Unauthorized
 *        '403':
 *          description: Forbidden
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: You are not authorized to view this resource
 *        '404':
 *          description: Not found
 *          content:
 *            application/json:
 *              schema:
 *                type: string
 *              example: Device not found
 *      security:
 *        - bearerAuth: []
 */
router.get('/devices/:id/audit', passport.authenticate(['jwt'], { session: false }), auditController.getByDeviceId)

module.exports = router
