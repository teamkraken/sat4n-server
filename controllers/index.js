const auditController = require('./audit.controller')
const authController = require('./auth.controller')
const deviceController = require('./device.controller')
const portController = require('./port.controller')
const roleController = require('./role.controller')
const userController = require('./user.controller')

module.exports = {
  auditController,
  authController,
  deviceController,
  portController,
  roleController,
  userController
}
