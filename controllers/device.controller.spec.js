/* eslint-env jest */
const controller = require('./device.controller')
const deviceService = require('../services/device.service')
const roleService = require('../services/role.service')
// const ssh = require('../services/ssh.service')

jest.mock('../services/ssh.service')
jest.mock('../services/device.service')

const mockDevice = {
  id: 5,
  name: 'c3560-test-1',
  ip: '1.1.1.2',
  uptime: '1 week, 12 hours, 24 minutes',
  model: 'WS-C3560-8PC',
  os: 'Cisco IOS Software',
  osVersion: '12.2(55)SE4',
  osImage: 'C3560-IPSERVICESK9-M',
  portCount: 9,
  location: 'Eir5;2E;Nethopur'
}

describe('Device Controller', () => {
  const res = {}
  res.json = (d) => { return d }
  res.send = (d) => { return d }
  res.status = () => res

  const user = {
    id: 1,
    role: {
      id: 1
    }
  }
  // let controllerTestDevice
  let testRole

  beforeAll(async () => {
    testRole = await roleService.create({ name: 'deviceControllerTest', resources: [] }, user)
  })

  afterAll(async () => {
    await roleService.delete(testRole.id, user)
  })

  it('Should get mockDevice', async () => {
    deviceService.get.mockResolvedValue(mockDevice)

    const get = await controller.get({ params: { id: mockDevice.id }, user }, res, null)

    expect(get.ip).toEqual(mockDevice.ip)
  })

  it('Should fail when user does not have access (get)', async () => {
    expect(await controller.get({
      user: {
        role: {
          id: testRole.id
        }
      },
      params: {
        id: 1
      }
    }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should fail when device does not exist (get)', async () => {
    const idDoesNotExist = 999
    deviceService.get.mockRejectedValue(new Error('Device not found'))

    controller.get({ params: { id: idDoesNotExist }, user }, res, null)
      .catch(err => {
        expect(err).toEqual('Device not found')
      })
  })

  it('Should get a list of all devices', async () => {
    deviceService.list.mockResolvedValue([mockDevice])

    const list = await controller.list({ user }, res, null)

    expect(list.length).toBeGreaterThan(0)
  })

  it('Should fail when user does not have access (list)', async () => {
    expect(await controller.list({
      user: {
        role: {
          id: testRole.id
        }
      }
    }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should create a new device', async () => {
    deviceService.create.mockResolvedValue(mockDevice)

    const create = await controller.create({ body: { ip: '1.1.1.2' }, user }, res, null)

    expect(create.ip).toEqual('1.1.1.2')
  })

  it('Should fail when user does not have access (create)', async () => {
    expect(await controller.create({
      body: {
        ip: '1.1.1.2'
      },
      user: {
        role: {
          id: testRole.id
        }
      }
    }, res, null)).toEqual('You are not authorized to view this resource')
  })

  it('Should fail when no ip is provided (create)', async () => {
    expect(await controller.create({
      body: {
        ip: ''
      },
      user: {
        role: {
          id: testRole.id
        }
      }
    }, res, null)).toEqual(new Error('Bad Request'))
  })

  it('Should fail when device already exists (create)', async () => {
    const ipAlreadyExists = '1.1.1.2'
    deviceService.create.mockRejectedValue(new Error('Device with this IP already exists'))

    controller.create({ body: { ip: ipAlreadyExists }, user }, res, null)
      .catch(err => {
        expect(err).toEqual('Device with this IP already exists')
      })
  })

  it('Should delete a device', async () => {
    deviceService.delete.mockResolvedValue(1)

    const success = await controller.delete({ params: { id: 5 }, user }, res, null)

    expect(success).toBe(1)
  })

  it('Should fail when user does not have access (delete)', async () => {
    expect(await controller.delete({
      params: {
        id: 5
      },
      user: {
        role: {
          id: testRole.id
        }
      }
    }, res, null)).toEqual(new Error('You are not authorized to view this resource'))
  })

  it('Should fail if id is not a number (delete)', async () => {
    expect(await controller.delete({
      params: {
        id: 'ada'
      },
      user: {
        role: {
          id: testRole.id
        }
      }
    }, res, null)).toEqual(new Error('Bad Request'))
  })

  it('Should fail when device does not exist (delete)', async () => {
    const idDoesNotExist = 999
    deviceService.delete.mockRejectedValue(new Error('Device not found'))

    controller.delete({ params: { id: idDoesNotExist }, user }, res, null)
      .catch(err => {
        expect(err).toEqual('Device not found')
      })
  })
})
