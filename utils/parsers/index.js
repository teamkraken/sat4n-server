const fs = require('fs')
const path = require('path')
const basename = path.basename(__filename)
const parsers = {}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js') && file.slice(-8) !== '.spec.js'
  })
  .forEach(file => {
    const parser = require(path.join(__dirname, file))

    parsers[parser.name] = parser
  })

module.exports = parsers
