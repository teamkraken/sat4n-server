'use strict'
const bcrypt = require('bcrypt')

module.exports = (sequelize, DataTypes) => {
  const UserSecret = sequelize.define('userSecret', {
    secret: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    }
  }, {})

  UserSecret.associate = function (models) {
    // associations can be defined here
    UserSecret.belongsTo(models.user)
  }

  UserSecret.beforeCreate(async (usersecret) => {
    // TODO: Move constant to config file
    const SALT_WORK_FACTOR = 12
    usersecret.secret = await bcrypt.hash(usersecret.secret, SALT_WORK_FACTOR)
  })

  UserSecret.validPassword = async (userPassword, dbPassword) => {
    const isValid = await bcrypt.compare(userPassword, dbPassword)
    return isValid
  }

  return UserSecret
}
