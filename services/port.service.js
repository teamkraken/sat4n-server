const Op = require('sequelize').Op
const { audit, device, port, vlan } = require('../models')
const { Cisco } = require('../services')
const service = require('./device.service')

module.exports = {
  update: async (deviceID, portID, vlanID, user) => {
    // Fetch live data from device
    await service.get(deviceID)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Fetch device from database
    return await device.findOne({
      where: { id: { [Op.eq]: deviceID } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: { id: { [Op.eq]: portID } }
        },
        {
          model: vlan,
          as: 'vlans',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          through: { attributes: [] },
          where: { id: { [Op.eq]: vlanID } }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then(async (d) => {
        if (d === null) {
          throw new Error('Port not found')
        }

        const { ports, vlans } = d

        // Restart port if we find one port that is not a trunk and not disabled
        if (ports.length === 1 && vlans.length === 1 && !isNaN(ports[0].vlan) && ports[0].status !== 'disabled') {
          // Perform port restart
          return await Cisco.updatePort(d.ip, ports[0].name, vlans[0].id)
            .then(async (response) => {
              // Audit port change
              const newAudit = {
                userId: user.id,
                action: 'Port:UPDATE',
                details: `${user.username} updated port ${ports[0].name} on ${d.name}.\nVlan before update: ${ports[0].vlan}\nVlan after update: ${vlanID}`,
                deviceId: deviceID
              }
              await audit.create(newAudit)
                .then((d) => { return d })
                .catch(/* istanbul ignore next */ (err) => { throw err })

              // Fetch the ports on the device to update
              const portsToUpdate = await port.findAll({ where: { deviceId: { [Op.eq]: d.id } } })
                .catch(/* istanbul ignore next */ (err) => { throw err })

              // Iterate through ports and update only ports where vlan or status has changed
              await response.interfacesStatus.map(async (p, i) => {
                // Find corresponding interfaces status to interfaces
                const foundPort = await response.interfaces.filter((int) => {
                  const firstHalf = int.interface.slice(0, 2)
                  const secondHalf = int.interface.replace(/[a-zA-Z]/g, '')
                  const name = `${firstHalf}${secondHalf}`

                  return name === p.name
                })

                if (p.name === portsToUpdate[i].name) {
                  await port.update(
                    {
                      ...p,
                      lastUsed: foundPort[0].output,
                      inputErrors: foundPort[0].inputErrors,
                      outputErrors: foundPort[0].outputErrors
                    },
                    { where: { id: { [Op.eq]: portsToUpdate[i].id } } }
                  )
                    .catch(/* istanbul ignore next */ (err) => { throw err })
                }
              })

              // Format vlans for db entry
              const newVlans = response.vlans.map((vlan) => { return { id: vlan.vlan, name: vlan.name } })

              // Get diff of vlans from switch and db
              const vlansDiff = newVlans.filter((nv) => d.vlans.map((v) => v.id).indexOf(parseInt(nv.id)) === -1)

              /* istanbul ignore next */
              if (newVlans.length !== d.vlans.length && vlansDiff.length !== 0) {
                // Try to insert vlans into db
                const vlans = await vlan.bulkCreate(newVlans, { returning: true, ignoreDuplicates: true })
                  .catch(/* istanbul ignore next */ (err) => { throw err })
                // Associate vlans to device
                await d.setVlans(vlans) // Sequelize deletes associations before setting assiciations like this (O.o)
                  .catch(/* istanbul ignore next */ (err) => { throw err })
              }

              // Return updated device from database
              return await device.findOne({
                where: { id: { [Op.eq]: deviceID } },
                include: [
                  {
                    model: port,
                    as: 'ports',
                    attributes: { exclude: ['createdAt', 'updatedAt', 'deviceId'] }
                  },
                  {
                    model: vlan,
                    as: 'vlans',
                    attributes: { exclude: ['createdAt', 'updatedAt'] },
                    through: { attributes: [] }
                  }
                ],
                attributes: { exclude: ['createdAt', 'updatedAt'] }
              })
                .then((returnDevice) => {
                  return ({
                    ...JSON.parse(JSON.stringify(returnDevice)), // Stripping away circular references
                    arpTable: response.arpTable,
                    ipSnoop: response.ipSnoop,
                    macTable: response.macTable,
                    neighbors: response.neighbors,
                    log: response.log
                  })
                })
                .catch(/* istanbul ignore next */ (err) => { throw err })
            })
            .catch(/* istanbul ignore next */ (err) => { throw err })
        } else {
          // Return error if condition was not met
          throw new Error('Cannot update trunks or disabled ports')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  restart: async (deviceID, portID, user) => {
    // Fetch live data from device
    await service.get(deviceID)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Fetch device from database
    return await device.findOne({
      where: { id: { [Op.eq]: deviceID } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: { id: { [Op.eq]: portID } }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then(async (d) => {
        if (d === null) {
          throw new Error('Port not found')
        }

        const { ports } = d

        // Restart port if we find one port that is not a trunk and not disabled
        if (ports.length === 1 && !isNaN(ports[0].vlan) && ports[0].status !== 'disabled') {
          // Perform port restart
          return await Cisco.restartPort(d.ip, ports[0].name)
            .then(async () => {
              // Audit port restart
              const newAudit = {
                userId: user.id,
                action: 'Port:RESTART',
                details: `${user.username} restarted port ${ports[0].name} on ${d.name}.`,
                deviceId: deviceID
              }

              await audit.create(newAudit)
                .catch(/* istanbul ignore next */ (err) => { throw err })

              return 1
            })
            .catch(/* istanbul ignore next */ (err) => { throw err })
        } else {
          // Return error if condition was not met
          throw new Error('Cannot restart trunks or disabled ports')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  disable: async (deviceID, portID, user) => {
    // Fetch live data from device
    await service.get(deviceID)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Fetch device from database
    return await device.findOne({
      where: { id: { [Op.eq]: deviceID } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: { id: { [Op.eq]: portID } }
        },
        {
          model: vlan,
          as: 'vlans',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          through: { attributes: [] }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then(async (d) => {
        if (d === null) {
          throw new Error('Port not found')
        }

        const { ports } = d

        // Restart port if we find one port that is not a trunk and not disabled
        if (ports.length === 1 && !isNaN(ports[0].vlan) && ports[0].status !== 'disabled') {
          // Perform port restart
          return await Cisco.disablePort(d.ip, ports[0].name)
            .then(async (response) => {
              // Audit port change
              const newAudit = {
                userId: user.id,
                action: 'Port:DISABLE',
                details: `${user.username} disabled port ${ports[0].name} on ${d.name}.`,
                deviceId: deviceID
              }
              audit.create(newAudit)
                .then((d) => { return d })
                .catch(/* istanbul ignore next */ (err) => { throw err })

              // Fetch the ports on the device to update
              const portsToUpdate = await port.findAll({ where: { deviceId: { [Op.eq]: d.id } } })

              // Iterate through ports and update only ports where vlan or status has changed
              await response.interfacesStatus.map(async (p, i) => {
                // Find corresponding interfaces status to interfaces
                const foundPort = await response.interfaces.filter((int) => {
                  const firstHalf = int.interface.slice(0, 2)
                  const secondHalf = int.interface.replace(/[a-zA-Z]/g, '')
                  const name = `${firstHalf}${secondHalf}`

                  return name === p.name
                })

                // if (p.name === dev.ports[i].name && (parseInt(p.vlan) !== dev.ports[i].vlan || p.status !== dev.ports[i].status)) {
                if (p.name === portsToUpdate[i].name) {
                  await port.update(
                    {
                      ...p,
                      lastUsed: foundPort[0].output,
                      inputErrors: foundPort[0].inputErrors,
                      outputErrors: foundPort[0].outputErrors
                    },
                    { where: { id: { [Op.eq]: portsToUpdate[i].id } } }
                  )
                    .catch(/* istanbul ignore next */ (err) => { throw err })
                }
              })

              // Format vlans for db entry
              const newVlans = response.vlans.map((vlan) => { return { id: vlan.vlan, name: vlan.name } })

              // Get diff of vlans from switch and db
              const vlansDiff = newVlans.filter((nv) => d.vlans.map((v) => v.id).indexOf(parseInt(nv.id)) === -1)

              /* istanbul ignore next */
              if (newVlans.length !== d.vlans.length && vlansDiff.length !== 0) {
                // Try to insert vlans into db
                const vlans = await vlan.bulkCreate(newVlans, { returning: true, ignoreDuplicates: true })
                  .catch(/* istanbul ignore next */ (err) => { throw err })
                // Associate vlans to device
                await d.setVlans(vlans) // Sequelize deletes associations before setting assiciations like this (O.o)
                  .catch(/* istanbul ignore next */ (err) => { throw err })
              }

              // Return updated device from database
              return await device.findOne({
                where: { id: { [Op.eq]: deviceID } },
                include: [
                  {
                    model: port,
                    as: 'ports',
                    attributes: { exclude: ['createdAt', 'updatedAt', 'deviceId'] }
                  },
                  {
                    model: vlan,
                    as: 'vlans',
                    attributes: { exclude: ['createdAt', 'updatedAt'] },
                    through: { attributes: [] }
                  }
                ],
                attributes: { exclude: ['createdAt', 'updatedAt'] }
              })
                .then((returnDevice) => {
                  return ({
                    ...JSON.parse(JSON.stringify(returnDevice)), // Stripping away circular references
                    arpTable: response.arpTable,
                    ipSnoop: response.ipSnoop,
                    macTable: response.macTable,
                    neighbors: response.neighbors,
                    log: response.log
                  })
                })
                .catch(/* istanbul ignore next */ (err) => { throw err })
            })
            .catch(/* istanbul ignore next */ (err) => { throw err })
        } else {
          // Return error if condition was not met
          throw new Error('Port not found')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  enable: async (deviceID, portID, user) => {
    // Fetch live data from device
    await service.get(deviceID)
      .catch(/* istanbul ignore next */ (err) => { throw err })

    // Fetch device from database
    return await device.findOne({
      where: { id: { [Op.eq]: deviceID } },
      include: [
        {
          model: port,
          as: 'ports',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          where: { id: { [Op.eq]: portID } }
        },
        {
          model: vlan,
          as: 'vlans',
          attributes: { exclude: ['createdAt', 'updatedAt'] },
          through: { attributes: [] }
        }
      ],
      attributes: { exclude: ['createdAt', 'updatedAt'] }
    })
      .then(async (d) => {
        if (d === null) {
          throw new Error('Port not found')
        }

        const { ports } = d

        // Restart port if we find one port that is not a trunk and not enabled
        if (ports.length === 1 && !isNaN(ports[0].vlan) && ports[0].status !== 'enabled') {
          // Perform port restart
          return await Cisco.enablePort(d.ip, ports[0].name)
            .then(async (response) => {
              // Audit port change
              const newAudit = {
                userId: user.id,
                action: 'Port:ENABLE',
                details: `${user.username} enabled port ${ports[0].name} on ${d.name}.`,
                deviceId: deviceID
              }
              audit.create(newAudit)
                .then((d) => { return d })
                .catch(/* istanbul ignore next */ (err) => { throw err })

              // Fetch the ports on the device to update
              const portsToUpdate = await port.findAll({ where: { deviceId: { [Op.eq]: d.id } } })

              // Iterate through ports and update only ports where vlan or status has changed
              await response.interfacesStatus.map(async (p, i) => {
                // Find corresponding interfaces status to interfaces
                const foundPort = await response.interfaces.filter((int) => {
                  const firstHalf = int.interface.slice(0, 2)
                  const secondHalf = int.interface.replace(/[a-zA-Z]/g, '')
                  const name = `${firstHalf}${secondHalf}`

                  return name === p.name
                })

                // if (p.name === dev.ports[i].name && (parseInt(p.vlan) !== dev.ports[i].vlan || p.status !== dev.ports[i].status)) {
                if (p.name === portsToUpdate[i].name) {
                  await port.update(
                    {
                      ...p,
                      lastUsed: foundPort[0].output,
                      inputErrors: foundPort[0].inputErrors,
                      outputErrors: foundPort[0].outputErrors
                    },
                    { where: { id: { [Op.eq]: portsToUpdate[i].id } } }
                  )
                    .catch(/* istanbul ignore next */ (err) => { throw err })
                }
              })

              // Format vlans for db entry
              const newVlans = response.vlans.map((vlan) => { return { id: vlan.vlan, name: vlan.name } })

              // Get diff of vlans from switch and db
              const vlansDiff = newVlans.filter((nv) => d.vlans.map((v) => v.id).indexOf(parseInt(nv.id)) === -1)

              /* istanbul ignore next */
              if (newVlans.length !== d.vlans.length && vlansDiff.length !== 0) {
                // Try to insert vlans into db
                const vlans = await vlan.bulkCreate(newVlans, { returning: true, ignoreDuplicates: true })
                  .catch(/* istanbul ignore next */ (err) => { throw err })
                // Associate vlans to device
                await d.setVlans(vlans) // Sequelize deletes associations before setting assiciations like this (O.o)
                  .catch(/* istanbul ignore next */ (err) => { throw err })
              }

              // Return updated device from database
              return await device.findOne({
                where: { id: { [Op.eq]: deviceID } },
                include: [
                  {
                    model: port,
                    as: 'ports',
                    attributes: { exclude: ['createdAt', 'updatedAt', 'deviceId'] }
                  },
                  {
                    model: vlan,
                    as: 'vlans',
                    attributes: { exclude: ['createdAt', 'updatedAt'] },
                    through: { attributes: [] }
                  }
                ],
                attributes: { exclude: ['createdAt', 'updatedAt'] }
              })
                .then((returnDevice) => {
                  return ({
                    ...JSON.parse(JSON.stringify(returnDevice)), // Stripping away circular references
                    arpTable: response.arpTable,
                    ipSnoop: response.ipSnoop,
                    macTable: response.macTable,
                    neighbors: response.neighbors,
                    log: response.log
                  })
                })
                .catch(/* istanbul ignore next */ (err) => { throw err })
            })
            .catch(/* istanbul ignore next */ (err) => { throw err })
        } else {
          // Return error if condition was not met
          throw new Error('Port not found')
        }
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  }
}
