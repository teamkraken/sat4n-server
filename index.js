#!/usr/bin/env node

const http = require('http')
const app = require('./app')

const port = 8080
app.set('port', port)

const server = http.createServer(app)
server.listen(port)
  .on('error', (e) => console.log(e))
  .on('listening', () => console.log(`Listening on port: ${port}`))
