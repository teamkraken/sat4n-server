const Router = require('express').Router
const swaggerUI = require('swagger-ui-express')
const swaggerJSdoc = require('swagger-jsdoc')

const router = Router()

const options = {
  definition: {
    openapi: '3.0.0', // Specification (optional, defaults to swagger: '2.0')
    info: {
      title: 'SAT:4N API', // Title (required)
      version: '1.0.0' // Version (required)
    },
    servers: [{
      url: '{protocol}://{server}:{port}/',
      description: 'some server description',
      variables: {
        protocol: {
          default: 'http'
        },
        server: {
          default: 'localhost'
        },
        port: {
          default: '8080'
        }
      }
    }],
    components: {
      securitySchemes: {
        bearerAuth: {
          type: 'http',
          scheme: 'bearer'
        }
      }
    }
  },
  // Path to the API docs
  apis: ['./routes/*.router.js']
}

const specs = swaggerJSdoc(options)

router.use('/', swaggerUI.serve, swaggerUI.setup(specs))
router.get('/swagger.json', (req, res) => { res.json(specs) })

module.exports = router
