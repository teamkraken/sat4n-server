const Op = require('sequelize').Op
const { audit, role, user, userSecret } = require('../models')

module.exports = {
  list: async () => {
    return await user.findAll({
      include: [{
        model: role,
        as: 'role',
        attributes: ['id', 'name']
      }],
      attributes: ['id', 'username']
    })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  },

  get: async (id) => {
    const usr = await user.findOne({
      where: { id: { [Op.eq]: id } },
      include: [{
        model: role,
        as: 'role',
        attributes: ['id', 'name']
      }],
      attributes: ['id', 'username']
    })
      .then((user) => {
        if (user === null) {
          throw new Error('User not found')
        }

        return user
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    if (usr === null) {
      throw new Error('User not found')
    }

    return usr
  },

  create: async (newUser, creator) => {
    // Check if user already exists
    await user.findOne({ where: { username: { [Op.eq]: newUser.username } } })
      .then((u) => {
        if (u !== null) {
          throw new Error('User with this username already exists')
        }
      })

    // Remove id received for new user
    delete newUser.id

    const nUserRole = await role.findOne({
      where: { id: { [Op.eq]: newUser.roleId } },
      attributes: ['id', 'name']
    })

    const nUser = await user.create(newUser)
      .then(async (u) => {
        await userSecret.create({
          secret: newUser.password,
          userId: u.id
        })

        const newAudit = {
          userId: creator.id,
          action: 'User:CREATE',
          details: `${creator.username} created user with username: ${u.username} and role: ${nUserRole.name}`
        }

        await audit.create(newAudit)
          .catch((err) => { throw err })

        return u
      })
      .catch((err) => { throw err })

    return { id: nUser.id, username: nUser.username, role: nUserRole }
  },

  update: async (newUser, userID, creator) => {
    // Remove id received in case it is not the same as userID
    delete newUser.id

    const oldUser = await user.findOne({
      where: { id: { [Op.eq]: userID } },
      attributes: {
        exclude: ['createdAt', 'updatedAt']
      }
    })

    await user.update(newUser,
      { where: { id: { [Op.eq]: userID } } })
      .then(async () => {
        /* istanbul ignore next */
        if (Object.keys(newUser).indexOf('password') !== -1 && newUser.password !== '') {
          await userSecret.create({
            secret: newUser.password,
            userId: userID
          })
        }

        const newAudit = {
          userId: creator.id,
          action: 'User:UPDATE',
          details: `${creator.username} updated user with username: ${oldUser.username}.
          \nUser before update: ${JSON.stringify(oldUser)}
          \nUser after update: ${JSON.stringify({ id: userID, username: newUser.username, enabled: true, roleId: newUser.roleId })}`
        }
        audit.create(newAudit)
          .catch((err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })

    const updatedUser = user.findOne({
      where: { id: { [Op.eq]: userID } },
      include: [{
        model: role,
        as: 'role',
        attributes: ['id', 'name']
      }],
      attributes: ['id', 'username']
    })

    return updatedUser
  },

  delete: async (id, creator) => {
    const u = await user.findOne({ where: { id: { [Op.eq]: id } } })
      .then((ret) => { return ret })
      .catch((err) => { throw err })

    return await user.destroy({
      where: { id: { [Op.eq]: id } }
    })
      .then(async (success) => {
        const newAudit = {
          userId: user.id,
          action: 'User:DELETE',
          details: `${creator.username} deleted user with username: ${u.username}`
        }
        return await audit.create(newAudit)
          .then(() => { return success })
          .catch((err) => { throw err })
      })
      .catch(/* istanbul ignore next */ (err) => { throw err })
  }
}
